package com.greendelta.olca.plugins.oekobaudat.rcp;

import java.io.File;

import org.openlca.app.util.Desktop;
import org.openlca.core.model.descriptors.FlowDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.greendelta.olca.plugins.oekobaudat.io.EpdStore;
import com.greendelta.olca.plugins.oekobaudat.model.DeclaredProduct;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDataSet;

public final class Browser {

	private Browser() {
	}

	public static void openFile(EpdDataSet dataSet, EpdStore store) {
		if (dataSet == null || dataSet.dataSetInfo == null
				|| store == null)
			return;
		Logger log = LoggerFactory.getLogger(Browser.class);
		log.trace("open EPD data set {}", dataSet);
		File dir = new File(Plugin.getEpdStore().baseDir, "ILCD");
		dir = new File(dir, "processes");
		File file = new File(dir, dataSet.dataSetInfo.uuid + ".xml");
		if (!file.exists()) {
			log.error("File " + file.getAbsolutePath() + " does not exist");
			return;
		}
		Desktop.browse(file.toURI().toString());
	}

	public static void openFile(DeclaredProduct product, EpdStore store) {
		if (product == null || product.flow == null || store == null)
			return;
		FlowDescriptor flow = product.flow;
		Logger log = LoggerFactory.getLogger(Browser.class);
		log.trace("open product data set {}", flow);
		File dir = new File(Plugin.getEpdStore().baseDir, "ILCD");
		dir = new File(dir, "flows");
		File file = new File(dir, flow.getRefId() + ".xml");
		if (!file.exists()) {
			log.error("File " + file.getAbsolutePath() + " does not exist");
			return;
		}
		Desktop.browse(file.toURI().toString());
	}

}
