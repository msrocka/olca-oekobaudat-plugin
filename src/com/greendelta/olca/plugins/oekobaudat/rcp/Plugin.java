package com.greendelta.olca.plugins.oekobaudat.rcp;

import java.util.Objects;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.openlca.app.db.Database;
import org.openlca.app.preferencepages.IoPreference;
import org.openlca.app.rcp.html.HtmlFolder;
import org.openlca.core.database.FlowDao;
import org.openlca.core.database.IDatabase;
import org.openlca.core.model.Flow;
import org.openlca.core.model.descriptors.FlowDescriptor;
import org.openlca.ilcd.io.DataStore;
import org.openlca.io.ilcd.output.ExportConfig;
import org.openlca.io.ilcd.output.FlowExport;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.greendelta.olca.plugins.oekobaudat.io.EpdStore;

public class Plugin extends AbstractUIPlugin implements BundleActivator {

	public static final String PLUGIN_ID = "oekobaudat";

	private Logger log = LoggerFactory.getLogger(getClass());

	private static Plugin plugin;
	private static EpdStore epdStore;

	public Plugin() {
	}

	public void start(BundleContext context) throws Exception {
		log.trace("start bundle {}", PLUGIN_ID);
		super.start(context);
		HtmlFolder.initialize(getBundle(), "html/html.zip");
		plugin = this;
	}

	public void stop(BundleContext context) throws Exception {
		log.trace("stop bundle {}", PLUGIN_ID);
		if (epdStore != null)
			epdStore.close();
		plugin = null;
		super.stop(context);
	}

	public static Plugin getDefault() {
		return plugin;
	}

	/**
	 * Get the used EpdStore of this plugin. Initializes the store if this is
	 * the first call or if the database changed. Returns null if there is no
	 * active database available.
	 */
	public static EpdStore getEpdStore() {
		IDatabase database = Database.get();
		if (database == null) {
			epdStore = null;
			return null;
		}
		if (epdStore == null) {
			epdStore = new EpdStore(database, PLUGIN_ID);
			EpdStore.lang = IoPreference.getIlcdLanguage();
			return epdStore;
		}
		if (Objects.equals(database, epdStore.database))
			return epdStore;
		try {
			epdStore.close();
			epdStore = new EpdStore(database, PLUGIN_ID);
			EpdStore.lang = IoPreference.getIlcdLanguage();
			return epdStore;
		} catch (Exception e) {
			plugin.log.error("failed to switch to new database");
			return null;
		}
	}

	/**
	 * Writes the given flow from the database as XML file to the ILCD folder if
	 * it does not exist there.
	 */
	public static void syncFlow(FlowDescriptor d) throws Exception {
		if (d == null)
			return;
		DataStore store = getEpdStore().ilcdStore;
		boolean exists = store.contains(org.openlca.ilcd.flows.Flow.class,
				d.getRefId());
		if (exists)
			return;
		plugin.log.trace("export flow to local ILCD store");
		IDatabase db = Database.get();
		FlowDao dao = new FlowDao(db);
		Flow flow = dao.getForRefId(d.getRefId());
		ExportConfig config = new ExportConfig(db, store);
		config.lang = IoPreference.getIlcdLanguage();
		FlowExport export = new FlowExport(config);
		export.run(flow);
	}
}
