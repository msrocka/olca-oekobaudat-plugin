package com.greendelta.olca.plugins.oekobaudat.rcp.ui.editor.product;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ImageHyperlink;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.openlca.app.App;
import org.openlca.app.components.TextDropComponent;
import org.openlca.app.db.Database;
import org.openlca.app.rcp.images.Images;
import org.openlca.app.util.Colors;
import org.openlca.app.util.Controls;
import org.openlca.app.util.FileType;
import org.openlca.app.util.UI;
import org.openlca.app.util.UIFactory;
import org.openlca.core.database.FlowDao;
import org.openlca.core.model.Flow;
import org.openlca.core.model.FlowProperty;
import org.openlca.core.model.ModelType;
import org.openlca.core.model.Unit;
import org.openlca.core.model.descriptors.ActorDescriptor;
import org.openlca.core.model.descriptors.FlowDescriptor;
import org.openlca.core.model.descriptors.SourceDescriptor;
import org.openlca.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.greendelta.olca.plugins.oekobaudat.model.DeclaredProduct;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDataSet;
import com.greendelta.olca.plugins.oekobaudat.rcp.Browser;
import com.greendelta.olca.plugins.oekobaudat.rcp.Messages;
import com.greendelta.olca.plugins.oekobaudat.rcp.Plugin;
import com.greendelta.olca.plugins.oekobaudat.rcp.ui.editor.EpdEditor;
import com.greendelta.olca.plugins.oekobaudat.rcp.ui.editor.VersionField;

public class DeclaredProductPage extends FormPage {

	private Logger log = LoggerFactory.getLogger(getClass());

	private EpdEditor editor;
	private FormToolkit toolkit;
	private DeclaredProduct product;

	public DeclaredProductPage(EpdEditor editor) {
		super(editor, "DeclaredProductPage", Messages.DeclaredProduct);
		EpdDataSet dataSet = editor.getDataSet();
		product = dataSet.declaredProduct;
		if (product == null) {
			product = new DeclaredProduct();
			dataSet.declaredProduct = product;
		}
		this.editor = editor;
	}

	@Override
	protected void createFormContent(IManagedForm managedForm) {
		toolkit = managedForm.getToolkit();
		ScrolledForm form = UI
				.formHeader(managedForm, Messages.DeclaredProduct);
		Composite body = UI.formBody(form, managedForm.getToolkit());
		createInfoSection(body);
		createVendorSection(body);
		createUnitSection(body);
		createPropertySection(body);
		form.reflow(true);
	}

	private void createInfoSection(Composite parent) {
		Composite comp = UI.formSection(parent, toolkit,
				Messages.GeneralInformation);
		Text uuidText = UI.formText(comp, toolkit, Messages.UUID);
		uuidText.setEditable(false);
		if (product.flow != null)
			uuidText.setText(product.flow.getRefId());
		createNameLink(comp);
		createFileLink(comp);
		createGenericFlowDrop(comp);
		createVersionField(comp);
		UI.filler(comp);
		Button button = toolkit.createButton(comp,
				Messages.ChangeProduct, SWT.NONE);
		Controls.onSelect(button, e -> ProductSwitch.run(editor));
	}

	private void createVersionField(Composite composite) {
		VersionField v = new VersionField(composite, toolkit);
		v.setVersion(product.version);
		editor.onSaved(() -> v.setVersion(product.version));
		v.onChange((version) -> {
			product.version = version;
			editor.setProductChanged();
		});
	}

	private void createGenericFlowDrop(Composite composite) {
		TextDropComponent flowDrop = UIFactory.createDropComponent(composite,
				Messages.GenericProduct, toolkit, ModelType.FLOW);
		flowDrop.setContent(product.genericFlow);
		flowDrop.setHandler((descriptor) -> {
			if (descriptor instanceof FlowDescriptor) {
				product.genericFlow = (FlowDescriptor) descriptor;
			} else {
				product.genericFlow = null;
			}
			editor.setProductChanged();
		});
	}

	private void createVendorSection(Composite body) {
		Composite composite = UI.formSection(body, toolkit,
				Messages.VendorInformation);
		Button check = UI.formCheckBox(composite, toolkit,
				Messages.IsVendorSpecific);
		Controls.onSelect(check, (e) -> {
			product.vendorSpecific = check.getSelection();
			editor.setProductChanged();
		});
		createVendorDrop(composite);
		createDocDrop(composite);
	}

	private void createVendorDrop(Composite composite) {
		TextDropComponent flowDrop = UIFactory.createDropComponent(composite,
				Messages.Vendor, toolkit, ModelType.ACTOR);
		flowDrop.setContent(product.vendor);
		flowDrop.setHandler((descriptor) -> {
			if (descriptor instanceof ActorDescriptor) {
				product.vendor = (ActorDescriptor) descriptor;
			} else {
				product.vendor = null;
			}
			editor.setProductChanged();
		});
	}

	private void createDocDrop(Composite composite) {
		TextDropComponent flowDrop = UIFactory.createDropComponent(composite,
				Messages.Documentation, toolkit, ModelType.SOURCE);
		flowDrop.setContent(product.documentation);
		flowDrop.setHandler((descriptor) -> {
			if (descriptor instanceof SourceDescriptor) {
				product.documentation = (SourceDescriptor) descriptor;
			} else {
				product.documentation = null;
			}
			editor.setProductChanged();
		});
	}

	private void createUnitSection(Composite body) {
		Composite composite = UI.formSection(body, toolkit,
				Messages.DeclaredUnit);
		Text amountText = UI.formText(composite, toolkit, Messages.Amount);
		amountText.setText(Double.toString(product.amount));
		amountText.addModifyListener((e) -> {
			try {
				String a = amountText.getText();
				if (Strings.nullOrEmpty(a))
					product.amount = (double) 0;
				else
					product.amount = Double.parseDouble(a);
				editor.setDirty(true);
			} catch (Exception ex) {
				log.error("not a valid number for product amount", e);
			}
		});
		Text unitText = UI.formText(composite, toolkit, Messages.Unit);
		unitText.setEditable(false);
		String unit = getUnit();
		if (unit != null)
			unitText.setText(unit);
	}

	private String getUnit() {
		try {
			FlowDescriptor descriptor = product.flow;
			if (descriptor == null)
				return null;
			FlowDao flowDao = new FlowDao(Database.get());
			Flow flow = flowDao.getForRefId(descriptor.getRefId());
			if (flow == null)
				return null;
			FlowProperty refProp = flow.getReferenceFlowProperty();
			if (refProp == null || refProp.getUnitGroup() == null)
				return null;
			Unit unit = refProp.getUnitGroup().getReferenceUnit();
			return unit == null ? null : unit.getName();
		} catch (Exception e) {
			log.error("Failed to get unit of declared product flow", e);
			return null;
		}
	}

	private void createNameLink(Composite composite) {
		UI.formLabel(composite, toolkit, Messages.Flow);
		final FlowDescriptor flow = product.flow;
		if (flow == null)
			return;
		ImageHyperlink link = toolkit.createImageHyperlink(composite, SWT.TOP);
		link.setForeground(Colors.linkBlue());
		link.setImage(Images.get(flow));
		link.setText(flow.getName());
		Controls.onClick(link, e -> {
			FlowDao dao = new FlowDao(Database.get());
			Flow realFlow = dao.getForRefId(flow.getRefId());
			App.openEditor(realFlow);
		});
	}

	private void createFileLink(Composite comp) {
		UI.formLabel(comp, toolkit, Messages.File);
		FlowDescriptor flow = product.flow;
		if (flow == null)
			return;
		ImageHyperlink link = toolkit.createImageHyperlink(comp, SWT.TOP);
		link.setForeground(Colors.linkBlue());
		link.setImage(Images.get(FileType.XML));
		link.setText("../flows/" + flow.getRefId() + ".xml");
		Controls.onClick(link,
				e -> Browser.openFile(product, Plugin.getEpdStore()));
	}

	private void createPropertySection(Composite parent) {
		Section section = UI.section(parent, toolkit,
				Messages.MaterialProperties);
		UI.gridData(section, true, true);
		Composite comp = UI.sectionClient(section, toolkit);
		UI.gridLayout(comp, 1);
		MaterialPropertyTable viewer = new MaterialPropertyTable(editor, comp);
		viewer.setInput(product.properties);
		viewer.bindTo(section);
	}

}
