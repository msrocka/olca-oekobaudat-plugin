package com.greendelta.olca.plugins.oekobaudat.rcp.ui.editor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.Window;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IPersistableElement;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.forms.editor.FormEditor;
import org.openlca.app.M;
import org.openlca.app.editors.IEditor;
import org.openlca.app.util.Editors;
import org.openlca.app.util.UI;
import org.openlca.core.model.Version;
import org.openlca.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.greendelta.olca.plugins.oekobaudat.model.AdminInfo;
import com.greendelta.olca.plugins.oekobaudat.model.DataSetInfo;
import com.greendelta.olca.plugins.oekobaudat.model.DeclaredProduct;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDataSet;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDescriptor;
import com.greendelta.olca.plugins.oekobaudat.rcp.Messages;
import com.greendelta.olca.plugins.oekobaudat.rcp.Plugin;
import com.greendelta.olca.plugins.oekobaudat.rcp.ui.editor.product.DeclaredProductPage;
import com.greendelta.olca.plugins.oekobaudat.rcp.ui.start.StartPageView;

public class EpdEditor extends FormEditor implements IEditor {

	private static final String ID = "epd.editor";

	private Logger log = LoggerFactory.getLogger(getClass());

	private EpdDataSet dataSet;
	private boolean dirty;
	private boolean productChanged;

	private List<Runnable> saveHandlers = new ArrayList<>();

	public static void open(EpdDescriptor descriptor) {
		if (!Plugin.getEpdStore().contains(descriptor)) {
			org.openlca.app.util.Error.showBox(Messages.EPD_DOWNLOAD_FAILED);
			return;
		}
		Editors.open(new EditorInput(descriptor), ID);
	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		super.init(site, input);
		setPartName(Strings.cut(input.getName(), 75));
		try {
			EditorInput editorInput = (EditorInput) input;
			dataSet = Plugin.getEpdStore().open(editorInput.descriptor);
		} catch (Exception e) {
			throw new PartInitException(
					"Failed to open editor: no correct input", e);
		}
	}

	public EpdDataSet getDataSet() {
		return dataSet;
	}

	@Override
	public void setDirty(boolean b) {
		if (dirty != b) {
			dirty = b;
			editorDirtyStateChanged();
		}
	}

	public void setProductChanged() {
		this.productChanged = true;
		setDirty(true);
	}

	@Override
	public boolean isDirty() {
		return dirty;
	}

	@Override
	protected void addPages() {
		try {
			addPage(new InfoPage(this));
			addPage(new ModelingPage(this));
			addPage(new AdminPage(this));
			addPage(new ModulePage(this));
			addPage(new DeclaredProductPage(this));
		} catch (Exception e) {
			log.error("failed to add editor page", e);
		}
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		try {
			updateVersion();
			Plugin.getEpdStore().save(dataSet);
			for (Runnable handler : saveHandlers) {
				handler.run();
			}
			setDirty(false);
			productChanged = false;
			StartPageView.refresh();
		} catch (Exception e) {
			log.error("failed to save EPD data set", e);
		}
	}

	private void updateVersion() {
		AdminInfo info = dataSet.adminInfo;
		if (info == null) {
			info = new AdminInfo();
			dataSet.adminInfo = info;
		}
		Version v = Version.fromString(info.version);
		v.incUpdate();
		info.version = v.toString();
		info.lastUpdate = new Date();
		if (!productChanged || dataSet.declaredProduct == null)
			return;
		DeclaredProduct product = dataSet.declaredProduct;
		v = Version.fromString(product.version);
		v.incUpdate();
		product.version = v.toString();
	}

	public void onSaved(Runnable handler) {
		saveHandlers.add(handler);
	}

	@Override
	public void doSaveAs() {
		InputDialog d = new InputDialog(UI.shell(), M.SaveAs,
				"#Save EPD as a new data set with the following name:",
				Messages.EPD + " " + M.Name, null);
		if (d.open() != Window.OK)
			return;
		String name = d.getValue();
		try {
			EpdDataSet clone = dataSet.clone();
			if (clone.dataSetInfo == null)
				clone.dataSetInfo = new DataSetInfo();
			clone.dataSetInfo.baseName = name;
			clone.dataSetInfo.uuid = UUID.randomUUID().toString();
			if (clone.adminInfo == null)
				clone.adminInfo = new AdminInfo();
			clone.adminInfo.version = Version.asString(0);
			clone.adminInfo.lastUpdate = new Date();
			Plugin.getEpdStore().save(clone);
			EpdEditor.open(clone.toDescriptor());
			StartPageView.refresh();
		} catch (Exception e) {
			log.error("failed to save EPD as new data set", e);
		}
	}

	@Override
	public boolean isSaveAsAllowed() {
		return true;
	}

	private static class EditorInput implements IEditorInput {

		private EpdDescriptor descriptor;

		public EditorInput(EpdDescriptor descriptor) {
			this.descriptor = descriptor;
		}

		@Override
		@SuppressWarnings("rawtypes")
		public Object getAdapter(Class adapter) {
			return null;
		}

		@Override
		public boolean exists() {
			return true;
		}

		@Override
		public ImageDescriptor getImageDescriptor() {
			return null;
		}

		@Override
		public String getName() {
			if (descriptor == null || descriptor.name == null)
				return "New EPD";
			else
				return descriptor.name;
		}

		@Override
		public IPersistableElement getPersistable() {
			return null;
		}

		@Override
		public String getToolTipText() {
			return getName();
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null)
				return false;
			if (obj == this)
				return true;
			if (!(obj instanceof EditorInput))
				return false;
			EditorInput other = (EditorInput) obj;
			return Objects.equals(this.descriptor, other.descriptor);
		}
	}
}
