package com.greendelta.olca.plugins.oekobaudat.rcp.ui.properties;

import java.util.List;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.openlca.app.util.UI;

import com.greendelta.olca.plugins.oekobaudat.model.MaterialProperty;
import com.greendelta.olca.plugins.oekobaudat.rcp.Messages;

class MaterialPropertyPage extends FormPage {

	private FormToolkit toolkit;
	private MaterialPropertyEditor editor;
	private List<MaterialProperty> properties;

	public MaterialPropertyPage(MaterialPropertyEditor editor,
			List<MaterialProperty> properties) {
		super(editor, "MaterialPropertyPage", Messages.MaterialProperties);
		this.editor = editor;
		this.properties = properties;
	}

	@Override
	protected void createFormContent(IManagedForm managedForm) {
		toolkit = managedForm.getToolkit();
		ScrolledForm form = UI.formHeader(managedForm,
				Messages.MaterialProperties);
		Composite body = UI.formBody(form, managedForm.getToolkit());
		createTable(body);
		form.reflow(true);
	}

	private void createTable(Composite parent) {
		Section section = UI.section(parent, toolkit,
				Messages.MaterialProperties);
		UI.gridData(section, true, true);
		Composite composite = UI.sectionClient(section, toolkit);
		UI.gridLayout(composite, 1);
		MaterialPropertyTable viewer = new MaterialPropertyTable(editor,
				composite);
		viewer.setInput(properties);
		viewer.bindTo(section);
	}

}