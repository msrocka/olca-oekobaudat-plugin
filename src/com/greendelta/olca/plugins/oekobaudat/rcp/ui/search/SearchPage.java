package com.greendelta.olca.plugins.oekobaudat.rcp.ui.search;

import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.progress.UIJob;
import org.openlca.app.App;
import org.openlca.app.rcp.html.HtmlFolder;
import org.openlca.app.rcp.html.WebPage;
import org.openlca.app.util.Desktop;
import org.openlca.app.util.Error;
import org.openlca.app.util.UI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.greendelta.olca.plugins.oekobaudat.io.Configs;
import com.greendelta.olca.plugins.oekobaudat.io.EpdStore;
import com.greendelta.olca.plugins.oekobaudat.io.ServerConfig;
import com.greendelta.olca.plugins.oekobaudat.io.ServerCredentials;
import com.greendelta.olca.plugins.oekobaudat.io.server.Connection;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDescriptor;
import com.greendelta.olca.plugins.oekobaudat.rcp.Messages;
import com.greendelta.olca.plugins.oekobaudat.rcp.Plugin;

import javafx.scene.web.WebEngine;

class SearchPage extends FormPage implements WebPage {

	private Logger log = LoggerFactory.getLogger(getClass());

	private Connection client;
	private WebEngine webkit;

	public SearchPage(SearchView view) {
		super(view, "SearchPage", Messages.OnlineSearch);
	}

	@Override
	public String getUrl() {
		return HtmlFolder.getUrl(Plugin.getDefault().getBundle(),
				"search_view.html");
	}

	@Override
	public void onLoaded(WebEngine webkit) {
		this.webkit = webkit;
		setMessages();
		UI.bindVar(webkit, "java", new JsHandler());
		tryConnect();
	}

	private void setMessages() {
		if (webkit == null)
			return;
		try {
			String messages = Messages.asJson();
			webkit.executeScript("setMessages(" + messages + ")");
		} catch (Exception e) {
			log.error("failed to set message strings", e);
		}
	}

	private void tryConnect() {
		ServerCredentials credentials = getCredentials();
		App.run(Messages.ConnectToSoda4LCA, () -> {
			try {
				client = Connection.create(credentials);
			} catch (Exception e) {
				log.error("failed to connect to network", e);
			}
		}, () -> {
			if (client != null)
				return;
			Error.showBox(Messages.NetworkConnectionFailed,
					Messages.NoSearchPossible);
		});
	}

	private ServerCredentials getCredentials() {
		EpdStore store = Plugin.getEpdStore();
		if (store == null)
			return null;
		ServerConfig config = Configs.getServerConfig(store);
		if (config == null)
			return null;
		else
			return config.downloadCredentials;
	}

	@Override
	protected void createFormContent(IManagedForm managedForm) {
		ScrolledForm form = UI.formHeader(managedForm, Messages.OnlineSearch);
		FormToolkit toolkit = managedForm.getToolkit();
		Composite body = UI.formBody(form, toolkit);
		body.setLayout(new FillLayout());
		UI.createWebView(body, this);
	}

	@Override
	public void dispose() {
		try {
			if (client != null) {
				log.trace("close network client");
				client.close();
			}
		} catch (Exception e) {
			log.error("failed to close network client", e);
		}
		super.dispose();
	}

	public class JsHandler {

		public void search(String term) {
			String t = term == null ? "" : term.trim();
			new SearchJob(t).schedule();
		}

		public void openModel(String json) {
			new OpenJob(json).schedule();
		}

		public void download(String json) {
			new DownloadJob(json, client).schedule();
		}
	}

	private class SearchJob extends UIJob {

		private String term;

		public SearchJob(String term) {
			super(Messages.Search);
			this.term = term;
		}

		@Override
		public IStatus runInUIThread(IProgressMonitor monitor) {
			log.trace("run search with {}", term);
			try {
				List<EpdDescriptor> result = client.search(term);
				String json = new Gson().toJson(result);
				String command = "setData(" + json + ")";
				webkit.executeScript(command);
				return Status.OK_STATUS;
			} catch (Exception e) {
				log.error("search failed", e);
				return Status.CANCEL_STATUS;
			}
		}
	}

	private class OpenJob extends UIJob {

		private String json;

		public OpenJob(String json) {
			super(Messages.OpenInBrowser);
			this.json = json;
		}

		@Override
		public IStatus runInUIThread(IProgressMonitor monitor) {
			try {
				EpdDescriptor d = new Gson().fromJson(json,
						EpdDescriptor.class);
				String url = client.credentials.url;
				url = url.replace("/resource",
						"/datasetdetail/process.xhtml?uuid=");
				url += d.refId;
				Desktop.browse(url);
				return Status.OK_STATUS;
			} catch (Exception e) {
				log.error("failed to open online data set in browser", e);
				return Status.CANCEL_STATUS;
			}
		}
	}
}