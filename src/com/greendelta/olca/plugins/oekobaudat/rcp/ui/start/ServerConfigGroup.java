package com.greendelta.olca.plugins.oekobaudat.rcp.ui.start;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.openlca.app.editors.DataBinding;
import org.openlca.app.util.Error;
import org.openlca.app.util.Info;
import org.openlca.app.util.UI;
import org.openlca.ilcd.descriptors.DataStock;
import org.openlca.ilcd.descriptors.DataStockList;
import org.openlca.ilcd.descriptors.DescriptorList;
import org.openlca.ilcd.io.NetworkClient;
import org.openlca.ilcd.processes.Process;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.greendelta.olca.plugins.oekobaudat.io.ServerCredentials;
import com.greendelta.olca.plugins.oekobaudat.rcp.Messages;

class ServerConfigGroup {

	private Logger log = LoggerFactory.getLogger(getClass());

	private final ServerCredentials credentials;

	private Text urlText;
	private Text userText;
	private Text passwordText;
	private Combo stockCombo;
	private DataBinding binding;
	private List<DataStock> dataStocks = new ArrayList<>();

	public ServerConfigGroup(ServerCredentials credentials) {
		this.credentials = credentials;
		binding = new DataBinding();
		if (credentials.dataStockName != null
				&& credentials.dataStockUuid != null) {
			DataStock dataStock = new DataStock();
			dataStock.uuid = credentials.dataStockUuid;
			dataStock.shortName = credentials.dataStockName;
			dataStocks.add(dataStock);
		}
	}

	void render(Composite parent, String title) {
		Group container = new Group(parent, SWT.NONE);
		UI.gridData(container, true, false);
		container.setText(title);
		UI.gridLayout(container, 2);
		createUrlText(container);
		createUserText(container);
		createPasswordText(container);
		createDataStockCombo(container);
		createTestButton(container);
	}

	private void createPasswordText(Composite container) {
		Label passwordLabel = new Label(container, SWT.NONE);
		passwordLabel.setText(Messages.Password);
		passwordText = new Text(container, SWT.BORDER | SWT.SINGLE);
		binding.onString(() -> credentials, "password", passwordText);
		UI.gridData(passwordText, true, false);
	}

	private void createUserText(Composite container) {
		Label userLabel = new Label(container, SWT.NONE);
		userLabel.setText(Messages.User);
		userText = new Text(container, SWT.BORDER | SWT.SINGLE);
		binding.onString(() -> credentials, "user", userText);
		UI.gridData(userText, true, false);
	}

	private void createUrlText(Composite container) {
		Label urlLabel = new Label(container, SWT.NONE);
		urlLabel.setText("URL");
		urlText = new Text(container, SWT.BORDER | SWT.SINGLE);
		binding.onString(() -> credentials, "url", urlText);
		UI.gridData(urlText, true, false);
	}

	private void createDataStockCombo(Group container) {
		Label label = new Label(container, SWT.NONE);
		label.setText(Messages.DataStock);
		Composite composite = new Composite(container, SWT.NONE);
		UI.gridData(composite, true, false);
		UI.gridLayout(composite, 2, 0, 0).horizontalSpacing = 5;
		stockCombo = new Combo(composite, SWT.NONE);
		stockCombo.addSelectionListener(new SelectionAction(
				this::newDataStoreSelected));
		UI.gridData(stockCombo, true, false);
		Button button = new Button(composite, SWT.NONE);
		button.setText(Messages.GetFromServer);
		button.addSelectionListener(new SelectionAction(this::loadDataStocks));
		updateStockCombo();
	}

	private void loadDataStocks() {
		log.trace("load data stocks from server");
		try (NetworkClient client = new NetworkClient(urlText.getText(),
				userText.getText(), passwordText.getText())) {
			client.connect();
			DataStockList dataStockList = client.getDataStockList();
			dataStocks.clear();
			if (dataStockList != null)
				dataStocks.addAll(dataStockList.dataStocks);
			updateStockCombo();
		} catch (Exception e) {
			log.error("failed to get data stocks", e);
			Error.showBox("Network connection failed",
					"Network connection failed with the following exception: "
							+ e.getMessage());
		}
	}

	private void updateStockCombo() {
		String[] items = new String[dataStocks.size() + 1];
		items[0] = "";
		int selectedItem = -1;
		for (int i = 0; i < dataStocks.size(); i++) {
			DataStock dataStock = dataStocks.get(i);
			String shortName = dataStock.shortName;
			items[i + 1] = shortName;
			if (Objects.equals(dataStock.uuid,
					credentials.dataStockUuid))
				selectedItem = i + 1;
		}
		stockCombo.setItems(items);
		if (selectedItem != -1)
			stockCombo.select(selectedItem);
	}

	private void newDataStoreSelected() {
		int idx = stockCombo.getSelectionIndex();
		if (idx < 1) {
			credentials.dataStockName = null;
			credentials.dataStockUuid = null;
			return;
		}
		if (idx > dataStocks.size())
			return;
		DataStock stock = dataStocks.get(idx - 1);
		if (stock.shortName != null)
			credentials.dataStockName = stock.shortName;
		credentials.dataStockUuid = stock.uuid;

	}

	private void createTestButton(Composite container) {
		new Label(container, SWT.NONE);
		Button testButton = new Button(container, SWT.PUSH);
		testButton.setText(Messages.TestConnection);
		testButton.addSelectionListener(new SelectionAction(
				this::testConnection));
	}

	private void testConnection() {
		log.trace("test server connection");
		try (NetworkClient client = new NetworkClient(urlText.getText(),
				userText.getText(), passwordText.getText())) {
			client.connect();
			String testString = "zement";
			log.trace("connected; test search for {}", testString);
			DescriptorList list = client.search(Process.class, testString);
			int size = list.descriptors.size();
			log.trace("found {} item(s)", size);
			Info.showBox("Network connection works", "Network connection works");
		} catch (Exception e) {
			log.error("test connection failed", e);
			Error.showBox("Network connection failed",
					"Network connection failed with the following exception: "
							+ e.getMessage());
		}
	}

	private class SelectionAction implements SelectionListener {

		private Runnable action;

		public SelectionAction(Runnable action) {
			this.action = action;
		}

		@Override
		public void widgetSelected(SelectionEvent e) {
			action.run();
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			widgetSelected(e);
		}
	}

}
