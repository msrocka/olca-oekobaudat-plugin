package com.greendelta.olca.plugins.oekobaudat.rcp.ui.editor.product;

import java.util.ArrayList;

import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.openlca.app.util.Numbers;
import org.openlca.app.util.UI;
import org.openlca.app.util.tables.Tables;
import org.openlca.app.viewers.table.AbstractTableViewer;
import org.openlca.app.viewers.table.modify.TextCellModifier;

import com.greendelta.olca.plugins.oekobaudat.model.MaterialProperty;
import com.greendelta.olca.plugins.oekobaudat.model.MaterialPropertyValue;
import com.greendelta.olca.plugins.oekobaudat.rcp.Messages;
import com.greendelta.olca.plugins.oekobaudat.rcp.ui.editor.EpdEditor;

class MaterialPropertyTable extends AbstractTableViewer<MaterialPropertyValue> {

	private final static String PROPERTY = Messages.Property;
	private final static String VALUE = Messages.Value;
	private final static String UNIT = Messages.Unit;

	private ArrayList<MaterialPropertyValue> values;
	private EpdEditor editor;

	public MaterialPropertyTable(EpdEditor editor, Composite parent) {
		super(parent);
		this.editor = editor;

		getModifySupport().bind(VALUE, new ValueModifier());
		Tables.bindColumnWidths(getViewer(), 0.34, 0.33, 0.33);
		getViewer().refresh(true);
	}

	@Override
	protected String[] getColumnHeaders() {
		return new String[] { PROPERTY, VALUE, UNIT };
	}

	@Override
	protected IBaseLabelProvider getLabelProvider() {
		return new Label();
	}

	public void setInput(ArrayList<MaterialPropertyValue> values) {
		this.values = values;
		if (values == null)
			setInput(new MaterialPropertyValue[0]);
		else
			setInput(values.toArray(new MaterialPropertyValue[values.size()]));
		getViewer().refresh(true);
	}

	@OnAdd
	protected void onCreate() {
		if (values == null)
			return;
		MaterialPropertyDialog dialog = new MaterialPropertyDialog(UI.shell());
		if (dialog.open() != Window.OK)
			return;
		MaterialProperty property = dialog.getSelectedProperty();
		if (property == null)
			return;
		MaterialPropertyValue value = new MaterialPropertyValue();
		value.property = property;
		value.value = (double) 1;
		values.add(value);
		setInput(values);
		editor.setProductChanged();
	}

	@OnRemove
	protected void onRemove() {
		for (MaterialPropertyValue property : getAllSelected())
			values.remove(property);
		setInput(values);
		editor.setProductChanged();
	}

	private class ValueModifier extends TextCellModifier<MaterialPropertyValue> {

		@Override
		protected String getText(MaterialPropertyValue element) {
			return String.valueOf(element.value);
		}

		@Override
		protected void setText(MaterialPropertyValue element, String text) {
			try {
				if (element == null)
					return;
				if (!text.equals(element.value)) {
					element.value = Double.valueOf(text);
					editor.setProductChanged();
				}
			} catch (NumberFormatException e) {

			}
		}
	}

	private class Label extends LabelProvider implements ITableLabelProvider {

		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			return null;
		}

		@Override
		public String getColumnText(Object element, int col) {
			if (!(element instanceof MaterialPropertyValue))
				return null;
			MaterialPropertyValue val = (MaterialPropertyValue) element;
			MaterialProperty prop = val.property;
			switch (col) {
			case 0:
				return prop != null ? prop.name : null;
			case 1:
				return Numbers.format(val.value);
			case 2:
				return prop != null ? prop.unit : null;
			default:
				return null;
			}
		}
	}
}
