package com.greendelta.olca.plugins.oekobaudat.rcp.ui.start;

import java.util.Objects;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.forms.editor.FormEditor;
import org.openlca.app.util.Editors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.greendelta.olca.plugins.oekobaudat.rcp.ui.SimpleEditorInput;

public class StartPageView extends FormEditor {

	private static final String ID = "epd.StartPageView";
	private Logger log = LoggerFactory.getLogger(getClass());
	private StartPage page;

	public static void open() {
		Editors.open(new SimpleEditorInput("EPD Editor"), ID);
	}

	public static void refresh() {
		StartPageView view = getCurrent();
		if (view == null || view.page == null)
			return;
		view.page.loadDescriptors();
	}

	private static StartPageView getCurrent() {
		IEditorReference editorRef = null;
		for (IEditorReference ref : Editors.getReferences()) {
			if (Objects.equals(ID, ref.getId())) {
				editorRef = ref;
			}
		}
		if (editorRef != null)
			return (StartPageView) editorRef.getEditor(false);
		else
			return null;
	}

	@Override
	protected void addPages() {
		try {
			page = new StartPage(this);
			addPage(page);
		} catch (Exception e) {
			log.error("failed to add page", e);
		}
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
	}

	@Override
	public void doSaveAs() {
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

}
