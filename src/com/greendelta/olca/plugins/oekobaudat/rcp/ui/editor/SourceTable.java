package com.greendelta.olca.plugins.oekobaudat.rcp.ui.editor;

import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.openlca.app.components.ModelSelectionDialog;
import org.openlca.app.rcp.images.Images;
import org.openlca.app.util.Actions;
import org.openlca.app.util.UI;
import org.openlca.app.util.tables.Tables;
import org.openlca.app.util.viewers.Viewers;
import org.openlca.core.model.ModelType;
import org.openlca.core.model.descriptors.BaseDescriptor;
import org.openlca.core.model.descriptors.SourceDescriptor;

import com.greendelta.olca.plugins.oekobaudat.rcp.Messages;

class SourceTable {

	private final List<SourceDescriptor> sources;
	private String title = Messages.Sources;
	private EpdEditor editor;

	private SourceTable(List<SourceDescriptor> sources) {
		this.sources = sources;
	}

	public static SourceTable createFor(List<SourceDescriptor> sources) {
		return new SourceTable(sources);
	}

	public SourceTable withEditor(EpdEditor editor) {
		this.editor = editor;
		return this;
	}

	public SourceTable withTitle(String title) {
		this.title = title;
		return this;
	}

	public void render(Composite parent, FormToolkit toolkit) {
		Section section = UI.section(parent, toolkit, title);
		Composite composite = UI.sectionClient(section, toolkit);
		UI.gridLayout(composite, 1);
		TableViewer viewer = Tables.createViewer(composite, Messages.Source);
		viewer.setLabelProvider(new SourceLabel());
		Tables.bindColumnWidths(viewer, 1);
		Action[] actions = createSourceActions(viewer);
		Actions.bind(section, actions);
		Actions.bind(viewer, actions);
		viewer.setInput(sources);
	}

	private Action[] createSourceActions(final TableViewer viewer) {
		Action[] actions = new Action[2];
		actions[0] = Actions.onAdd(() -> {
			BaseDescriptor descriptor = ModelSelectionDialog
					.select(ModelType.SOURCE);
			if (!(descriptor instanceof SourceDescriptor))
				return;
			sources.add((SourceDescriptor) descriptor);
			viewer.setInput(sources);
			if (editor != null)
				editor.setDirty(true);
		});
		actions[1] = Actions.onRemove(() -> {
			SourceDescriptor descriptor = Viewers.getFirstSelected(viewer);
			if (descriptor == null)
				return;
			sources.remove(descriptor);
			viewer.setInput(sources);
			if (editor != null)
				editor.setDirty(true);
		});
		return actions;
	}

	private class SourceLabel extends LabelProvider implements
			ITableLabelProvider {

		@Override
		public Image getColumnImage(Object obj, int col) {
			if (!(obj instanceof SourceDescriptor))
				return null;
			return Images.get((SourceDescriptor) obj);
		}

		@Override
		public String getColumnText(Object obj, int col) {
			if (!(obj instanceof SourceDescriptor))
				return null;
			SourceDescriptor descriptor = (SourceDescriptor) obj;
			return descriptor.getName();
		}
	}

}
