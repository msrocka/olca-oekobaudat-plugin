package com.greendelta.olca.plugins.oekobaudat.rcp.ui.editor;

import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.openlca.app.components.ModelSelectionDialog;
import org.openlca.app.rcp.images.Images;
import org.openlca.app.util.Actions;
import org.openlca.app.util.UI;
import org.openlca.app.util.tables.Tables;
import org.openlca.app.util.viewers.Viewers;
import org.openlca.core.model.ModelType;
import org.openlca.core.model.descriptors.ActorDescriptor;
import org.openlca.core.model.descriptors.BaseDescriptor;

class ActorTable {

	private final List<ActorDescriptor> actors;
	private String title = "Actors";
	private EpdEditor editor;

	private ActorTable(List<ActorDescriptor> actors) {
		this.actors = actors;
	}

	public static ActorTable createFor(List<ActorDescriptor> actors) {
		return new ActorTable(actors);
	}

	public ActorTable withEditor(EpdEditor editor) {
		this.editor = editor;
		return this;
	}

	public ActorTable withTitle(String title) {
		this.title = title;
		return this;
	}

	public TableViewer render(Composite parent, FormToolkit toolkit) {
		Section section = UI.section(parent, toolkit, title);
		Composite composite = UI.sectionClient(section, toolkit);
		UI.gridLayout(composite, 1);
		TableViewer viewer = Tables.createViewer(composite, title);
		viewer.setLabelProvider(new ActorLabel());
		Tables.bindColumnWidths(viewer, 1);
		Action[] actions = createActions(viewer);
		Actions.bind(section, actions);
		Actions.bind(viewer, actions);
		viewer.setInput(actors);
		return viewer;
	}

	private Action[] createActions(final TableViewer viewer) {
		Action[] actions = new Action[2];
		actions[0] = Actions.onAdd(() -> {
			BaseDescriptor descriptor = ModelSelectionDialog
					.select(ModelType.ACTOR);
			if (!(descriptor instanceof ActorDescriptor))
				return;
			actors.add((ActorDescriptor) descriptor);
			viewer.setInput(actors);
			if (editor != null)
				editor.setDirty(true);
		});
		actions[1] = Actions.onRemove(() -> {
			ActorDescriptor descriptor = Viewers.getFirstSelected(viewer);
			if (descriptor == null)
				return;
			actors.remove(descriptor);
			viewer.setInput(actors);
			if (editor != null)
				editor.setDirty(true);
		});
		return actions;
	}

	private class ActorLabel extends LabelProvider implements
			ITableLabelProvider {

		@Override
		public Image getColumnImage(Object obj, int col) {
			if (!(obj instanceof ActorDescriptor))
				return null;
			return Images.get((ActorDescriptor) obj);
		}

		@Override
		public String getColumnText(Object obj, int col) {
			if (!(obj instanceof ActorDescriptor))
				return null;
			ActorDescriptor d = (ActorDescriptor) obj;
			return d.getName();
		}
	}
}
