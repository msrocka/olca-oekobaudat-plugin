package com.greendelta.olca.plugins.oekobaudat.rcp.ui.editor.product;

import java.util.Date;

import org.openlca.app.components.ModelSelectionDialog;
import org.openlca.app.util.Error;
import org.openlca.app.util.Question;
import org.openlca.core.model.FlowType;
import org.openlca.core.model.ModelType;
import org.openlca.core.model.Version;
import org.openlca.core.model.descriptors.BaseDescriptor;
import org.openlca.core.model.descriptors.FlowDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.greendelta.olca.plugins.oekobaudat.io.conversion.Converter;
import com.greendelta.olca.plugins.oekobaudat.model.AdminInfo;
import com.greendelta.olca.plugins.oekobaudat.model.DeclaredProduct;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDataSet;
import com.greendelta.olca.plugins.oekobaudat.rcp.Messages;
import com.greendelta.olca.plugins.oekobaudat.rcp.Plugin;
import com.greendelta.olca.plugins.oekobaudat.rcp.ui.editor.EpdEditor;

class ProductSwitch {

	private EpdEditor editor;

	private ProductSwitch(EpdEditor editor) {
		this.editor = editor;
	}

	public static void run(EpdEditor editor) {
		new ProductSwitch(editor).run();
	}

	private void run() {
		BaseDescriptor d = ModelSelectionDialog.select(ModelType.FLOW);
		if (!(d instanceof FlowDescriptor))
			return;
		FlowDescriptor flow = (FlowDescriptor) d;
		if (flow.getFlowType() != FlowType.PRODUCT_FLOW) {
			Error.showBox(Messages.NotAProduct, Messages.NotAProduct_Info);
			return;
		}
		boolean b = Question.ask(Messages.SaveWithNewProduct,
				Messages.SaveWithNewProduct_Question);
		if (!b)
			return;
		doIt(flow);
	}

	private void doIt(FlowDescriptor flow) {
		try {
			EpdDataSet ds = editor.getDataSet();
			Plugin.getEpdStore().save(ds);
			editor.close(false);
			switchProduct(flow, ds);
			updateVersion(ds);
			Plugin.getEpdStore().save(ds);
			EpdEditor.open(ds.toDescriptor());
		} catch (Exception e) {
			Logger log = LoggerFactory.getLogger(getClass());
			log.error("failed to switch data set product", e);
		}
	}

	private void switchProduct(FlowDescriptor flow, EpdDataSet ds)
			throws Exception {
		Plugin.syncFlow(flow);
		DeclaredProduct dp = new DeclaredProduct();
		if (ds.declaredProduct != null) {
			dp.amount = ds.declaredProduct.amount;
		}
		dp.flow = flow;
		ds.declaredProduct = dp;
		Converter.readProductData(ds, Plugin.getEpdStore());
	}

	private void updateVersion(EpdDataSet ds) {
		AdminInfo info = ds.adminInfo;
		if (info == null) {
			info = new AdminInfo();
			ds.adminInfo = info;
		}
		Version v = Version.fromString(info.version);
		v.incUpdate();
		info.version = v.toString();
		info.lastUpdate = new Date();
	}
}
