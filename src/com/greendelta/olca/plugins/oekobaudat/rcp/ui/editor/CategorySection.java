package com.greendelta.olca.plugins.oekobaudat.rcp.ui.editor;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.openlca.app.components.FileChooser;
import org.openlca.app.rcp.images.Icon;
import org.openlca.app.util.Actions;
import org.openlca.app.util.UI;
import org.openlca.app.util.tables.Tables;
import org.openlca.app.util.viewers.Viewers;
import org.openlca.ilcd.commons.Class;
import org.openlca.ilcd.commons.Classification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.greendelta.olca.plugins.oekobaudat.io.EpdStore;
import com.greendelta.olca.plugins.oekobaudat.model.DataSetInfo;
import com.greendelta.olca.plugins.oekobaudat.rcp.Messages;
import com.greendelta.olca.plugins.oekobaudat.rcp.Plugin;

class CategorySection {

	private Logger log = LoggerFactory.getLogger(getClass());

	private EpdEditor editor;
	private DataSetInfo info;

	public CategorySection(EpdEditor editor, DataSetInfo info) {
		this.editor = editor;
		this.info = info;
	}

	public void render(Composite parent, FormToolkit toolkit) {
		Section section = UI.section(parent, toolkit, Messages.Classification);
		Composite composite = UI.sectionClient(section, toolkit);
		UI.gridLayout(composite, 1);
		TableViewer viewer = Tables.createViewer(composite,
				Messages.ClassificationSystem,
				Messages.CategoryPath);
		viewer.setLabelProvider(new RowLabel());
		viewer.setInput(info.classifications);
		Tables.bindColumnWidths(viewer, 0.3, 0.7);
		bindActions(section, viewer);
	}

	private void bindActions(Section section, TableViewer viewer) {
		Action importAction = Actions.create(
				Messages.ImportClassificationFile,
				Icon.IMPORT.descriptor(),
				() -> importFile());
		Action add = Actions.onAdd(() -> addRow(viewer));
		Action delete = Actions.onRemove(() -> deleteRow(viewer));
		Actions.bind(section, importAction, add, delete);
		Actions.bind(viewer, add, delete);
	}

	private void addRow(TableViewer viewer) {
		CategoryDialog dialog = new CategoryDialog(UI.shell());
		if (dialog.open() != Window.OK)
			return;
		Classification classification = dialog.getSelection();
		if (classification == null)
			return;
		info.classifications.add(classification);
		viewer.setInput(info.classifications);
		editor.setDirty(true);
	}

	private void deleteRow(TableViewer viewer) {
		Classification classification = Viewers.getFirstSelected(viewer);
		if (classification == null)
			return;
		info.classifications.remove(classification);
		viewer.setInput(info.classifications);
		editor.setDirty(true);
	}

	private void importFile() {
		File file = FileChooser.forImport("*.xml");
		if (file == null)
			return;
		try {
			EpdStore store = Plugin.getEpdStore();
			File rootDir = store.ilcdStore.getRootFolder();
			if (!rootDir.exists())
				return;
			File dir = new File(rootDir, "classifications");
			if (!dir.exists())
				dir.mkdirs();
			File localFile = new File(dir, file.getName());
			Files.copy(file.toPath(), localFile.toPath(),
					StandardCopyOption.REPLACE_EXISTING);
		} catch (Exception e) {
			log.error("failed to import classification file", e);
		}
	}

	private class RowLabel extends LabelProvider implements
			ITableLabelProvider {

		@Override
		public Image getColumnImage(Object element, int col) {
			return null;
		}

		@Override
		public String getColumnText(Object element, int col) {
			if (!(element instanceof Classification))
				return null;
			Classification classification = (Classification) element;
			switch (col) {
			case 0:
				return classification.name;
			case 1:
				return getPath(classification);
			default:
				return null;
			}
		}

		private String getPath(Classification classification) {
			List<org.openlca.ilcd.commons.Class> classes = classification.classes;
			classification.classes.sort((c1, c2) -> {
				if (c1.level == null || c2.level == null)
					return 0;
				int i1 = c1.level.intValue();
				int i2 = c2.level.intValue();
				return i1 - i2;
			});
			StringBuilder path = new StringBuilder();
			for (int i = 0; i < classes.size(); i++) {
				Class clazz = classes.get(i);
				if (clazz.classId != null)
					path.append(clazz.classId).append(" ");
				path.append(clazz.value);
				if (i < (classes.size() - 1))
					path.append(" / ");
			}
			return path.toString();
		}
	}

}
