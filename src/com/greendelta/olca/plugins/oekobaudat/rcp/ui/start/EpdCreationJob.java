package com.greendelta.olca.plugins.oekobaudat.rcp.ui.start;

import java.util.Date;
import java.util.UUID;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.progress.UIJob;
import org.openlca.core.model.descriptors.FlowDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.greendelta.olca.plugins.oekobaudat.io.conversion.FlowDecorator;
import com.greendelta.olca.plugins.oekobaudat.model.AdminInfo;
import com.greendelta.olca.plugins.oekobaudat.model.DataSetInfo;
import com.greendelta.olca.plugins.oekobaudat.model.DeclaredProduct;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDataSet;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDescriptor;
import com.greendelta.olca.plugins.oekobaudat.model.ModellingInfo;
import com.greendelta.olca.plugins.oekobaudat.rcp.Plugin;
import com.greendelta.olca.plugins.oekobaudat.rcp.ui.editor.EpdEditor;

public class EpdCreationJob extends UIJob {

	private Logger log = LoggerFactory.getLogger(getClass());

	private final String epdName;
	private final FlowDescriptor flowDescriptor;

	public EpdCreationJob(String epdName, FlowDescriptor product) {
		super("Create EPD " + epdName);
		this.epdName = epdName;
		this.flowDescriptor = product;
	}

	@Override
	public IStatus runInUIThread(IProgressMonitor monitor) {
		log.trace("Create EPD {}", epdName);
		try {
			monitor.beginTask("Create EPD", IProgressMonitor.UNKNOWN);
			if (epdName == null || flowDescriptor == null) {
				log.error("EPD name or product is null");
				return Status.CANCEL_STATUS;
			}
			Plugin.syncFlow(flowDescriptor);
			EpdDescriptor descriptor = createEpd();
			EpdEditor.open(descriptor);
			StartPageView.refresh();
			monitor.done();
			return Status.OK_STATUS;
		} catch (Exception e) {
			log.error("Failed to create EPD data set", e);
			return Status.CANCEL_STATUS;
		}
	}

	private EpdDescriptor createEpd() throws Exception {
		String refId = UUID.randomUUID().toString();
		EpdDataSet dataSet = new EpdDataSet();
		DataSetInfo info = new DataSetInfo();
		dataSet.dataSetInfo = info;
		DeclaredProduct declaredProduct = new DeclaredProduct();
		declaredProduct.flow = flowDescriptor;
		new FlowDecorator(declaredProduct, Plugin.getEpdStore()).read();
		dataSet.declaredProduct = declaredProduct;
		info.baseName = epdName;
		info.uuid = refId;
		AdminInfo adminInfo = new AdminInfo();
		dataSet.adminInfo = adminInfo;
		adminInfo.lastUpdate = new Date();
		ModellingInfo modellingInfo = new ModellingInfo();
		dataSet.modellingInfo = modellingInfo;
		Plugin.getEpdStore().save(dataSet);
		return dataSet.toDescriptor();
	}

}
