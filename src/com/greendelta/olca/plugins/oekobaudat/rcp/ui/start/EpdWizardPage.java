package com.greendelta.olca.plugins.oekobaudat.rcp.ui.start;

import org.eclipse.jface.dialogs.DialogPage;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.openlca.app.M;
import org.openlca.app.navigation.INavigationElement;
import org.openlca.app.navigation.ModelTextFilter;
import org.openlca.app.navigation.NavigationTree;
import org.openlca.app.navigation.Navigator;
import org.openlca.app.navigation.filters.EmptyCategoryFilter;
import org.openlca.app.navigation.filters.FlowTypeFilter;
import org.openlca.app.util.UI;
import org.openlca.app.util.viewers.Viewers;
import org.openlca.core.model.FlowType;
import org.openlca.core.model.ModelType;
import org.openlca.core.model.descriptors.FlowDescriptor;

import com.greendelta.olca.plugins.oekobaudat.rcp.Messages;

class EpdWizardPage extends WizardPage {

	private Text nameText;
	private Text filterText;
	private FlowDescriptor flow;
	private TreeViewer productViewer;

	public EpdWizardPage() {
		super(Messages.CreateANewEPD);
		setTitle(Messages.CreateANewEPD);
		setDescription(Messages.CreateANewEPD_Description);
		setPageComplete(false);
	}

	public String getEpdName() {
		return nameText.getText();
	}

	public FlowDescriptor getFlow() {
		return flow;
	}

	@Override
	public void createControl(Composite parent) {
		Composite composite = UI.formComposite(parent);
		nameText = UI.formText(composite, Messages.Name);
		nameText.addModifyListener((e) -> validateInput());
		filterText = UI.formText(composite, M.Filter);
		UI.formLabel(composite, M.Product);
		createProductViewer(new Composite(composite, SWT.NONE));
		setControl(composite);
		setPageComplete(false);
	}

	private void createProductViewer(Composite composite) {
		UI.gridData(composite, true, false);
		composite.setLayout(getViewerLayout());
		productViewer = NavigationTree.createViewer(composite);
		UI.gridData(productViewer.getTree(), true, true).heightHint = 200;
		productViewer.addFilter(new FlowTypeFilter(FlowType.ELEMENTARY_FLOW,
				FlowType.WASTE_FLOW));
		productViewer.addFilter(new EmptyCategoryFilter());
		productViewer.addFilter(new ModelTextFilter(filterText, productViewer));
		productViewer.setInput(Navigator.findElement(ModelType.FLOW));
		productViewer.addSelectionChangedListener((e) -> setSelectedProduct());
	}

	private void setSelectedProduct() {
		INavigationElement<?> element = Viewers.getFirstSelected(productViewer);
		if (element == null)
			return;
		Object content = element.getContent();
		if (!(content instanceof FlowDescriptor))
			return;
		flow = (FlowDescriptor) content;
		validateInput();
	}

	private GridLayout getViewerLayout() {
		GridLayout layout = new GridLayout(1, true);
		layout.verticalSpacing = 0;
		layout.marginWidth = 0;
		layout.marginHeight = 0;
		layout.horizontalSpacing = 0;
		return layout;
	}

	private void validateInput() {
		String name = nameText.getText();
		if (name == null || name.trim().isEmpty()) {
			error("No name selected");
			return;
		}
		if (flow == null) {
			error("No product selected");
			return;
		}
		setPageComplete(true);
		setMessage(null);
	}

	private void error(String string) {
		setMessage(string, DialogPage.ERROR);
		setPageComplete(false);
	}

}
