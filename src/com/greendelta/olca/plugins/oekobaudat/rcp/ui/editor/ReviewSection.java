package com.greendelta.olca.plugins.oekobaudat.rcp.ui.editor;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.openlca.app.components.TextDropComponent;
import org.openlca.app.rcp.images.Icon;
import org.openlca.app.util.Actions;
import org.openlca.app.util.UI;
import org.openlca.app.util.UIFactory;
import org.openlca.app.util.viewers.Viewers;
import org.openlca.core.model.ModelType;
import org.openlca.core.model.descriptors.SourceDescriptor;
import org.openlca.ilcd.commons.ReviewType;

import com.greendelta.olca.plugins.oekobaudat.model.ModellingInfo;
import com.greendelta.olca.plugins.oekobaudat.model.Review;
import com.greendelta.olca.plugins.oekobaudat.rcp.Messages;

class ReviewSection {

	private ModellingInfo modellingInfo;
	private EpdEditor editor;

	private Composite parent;
	private FormToolkit toolkit;
	private ScrolledForm form;

	public ReviewSection(ModellingInfo modellingInfo, EpdEditor editor) {
		this.modellingInfo = modellingInfo;
		this.editor = editor;
	}

	public void render(Composite body, FormToolkit toolkit, ScrolledForm form) {
		this.toolkit = toolkit;
		this.form = form;
		Section section = UI.section(body, toolkit, Messages.Reviews);
		parent = UI.sectionClient(section, toolkit);
		UI.gridLayout(parent, 1);
		for (Review review : modellingInfo.reviews)
			new Sec(review);
		Action addAction = Actions.create(Messages.AddReview,
				Icon.ADD.descriptor(), this::addReview);
		Actions.bind(section, addAction);
		form.reflow(true);
	}

	private void addReview() {
		Review review = new Review();
		modellingInfo.reviews.add(review);
		new Sec(review);
		form.reflow(true);
		editor.setDirty(true);
	}

	private class Sec {

		private Review review;
		private Section section;

		Sec(Review model) {
			this.review = model;
			createUi();
		}

		private void createUi() {
			int idx = modellingInfo.reviews.indexOf(review) + 1;
			section = UI.section(parent, toolkit, Messages.Review + " " + idx);
			Composite composite = UI.sectionClient(section, toolkit);
			UI.gridLayout(composite, 1);
			Composite formComposite = UI.formComposite(composite, toolkit);
			UI.gridData(formComposite, true, false);
			createTypeViewer(formComposite);
			createDetailsText(formComposite);
			createReportText(formComposite);
			createActorTable(composite);
			Action deleteAction = Actions.create(Messages.DeleteReview,
					Icon.DELETE.descriptor(), this::delete);
			Actions.bind(section, deleteAction);
		}

		private void createReportText(Composite composite) {
			TextDropComponent textDrop = UIFactory.createDropComponent(
					composite, Messages.CompleteReviewReport, toolkit,
					ModelType.SOURCE);
			textDrop.setContent(review.reviewReport);
			textDrop.setHandler((d) -> {
				if (!(d instanceof SourceDescriptor))
					review.reviewReport = null;
				else
					review.reviewReport = (SourceDescriptor) d;
				editor.setDirty(true);
			});
		}

		private void createActorTable(Composite composite) {
			ActorTable.createFor(review.reviewers)
					.withEditor(editor)
					.withTitle(Messages.Reviewer)
					.render(composite, toolkit);
		}

		private void createDetailsText(Composite composite) {
			Text text = UI.formMultiText(composite, toolkit,
					Messages.ReviewDetails);
			if (review.details != null)
				text.setText(review.details);
			text.addModifyListener((e) -> {
				review.details = text.getText();
				editor.setDirty(true);
			});
		}

		private void createTypeViewer(Composite composite) {
			UI.formLabel(composite, Messages.ReviewType);
			ComboViewer viewer = new ComboViewer(composite);
			UI.gridData(viewer.getControl(), true, false);
			viewer.setContentProvider(ArrayContentProvider.getInstance());
			viewer.setLabelProvider(new TypeLabel());
			viewer.setInput(ReviewType.values());
			if (review.type != null) {
				ISelection s = new StructuredSelection(review.type);
				viewer.setSelection(s);
			}
			viewer.addSelectionChangedListener((e) -> {
				ReviewType type = Viewers.getFirst(e.getSelection());
				review.type = type;
				editor.setDirty(true);
			});
		}

		private void delete() {
			modellingInfo.reviews.remove(review);
			section.dispose();
			form.reflow(true);
			editor.setDirty(true);
		}
	}

	private class TypeLabel extends LabelProvider {

		@Override
		public String getText(Object element) {
			if (!(element instanceof ReviewType))
				return null;
			ReviewType type = (ReviewType) element;
			switch (type) {
			case ACCREDITED_THIRD_PARTY_REVIEW:
				return Messages.AccreditedThirdPartyReview;
			case DEPENDENT_INTERNAL_REVIEW:
				return Messages.DependentInternalReview;
			case INDEPENDENT_EXTERNAL_REVIEW:
				return Messages.IndependentExternalReview;
			case INDEPENDENT_INTERNAL_REVIEW:
				return Messages.IndependentInternalReview;
			case INDEPENDENT_REVIEW_PANEL:
				return Messages.IndependentReviewPanel;
			case NOT_REVIEWED:
				return Messages.NotReviewed;
			default:
				return null;
			}
		}
	}
}
