package com.greendelta.olca.plugins.oekobaudat.rcp.ui.editor;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.ui.part.EditorActionBarContributor;
import org.openlca.app.App;
import org.openlca.app.rcp.images.Icon;
import org.openlca.app.util.Editors;
import org.openlca.app.util.Info;
import org.openlca.app.util.Question;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.greendelta.olca.plugins.oekobaudat.io.Configs;
import com.greendelta.olca.plugins.oekobaudat.io.EpdStore;
import com.greendelta.olca.plugins.oekobaudat.io.ServerConfig;
import com.greendelta.olca.plugins.oekobaudat.io.ServerCredentials;
import com.greendelta.olca.plugins.oekobaudat.io.server.Connection;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDataSet;
import com.greendelta.olca.plugins.oekobaudat.rcp.Messages;
import com.greendelta.olca.plugins.oekobaudat.rcp.Plugin;

public class EditorActionBar extends EditorActionBarContributor {

	@Override
	public void contributeToToolBar(IToolBarManager toolBarManager) {
		toolBarManager.add(new UploadAction());
	}

	private class UploadAction extends Action {

		private Logger log = LoggerFactory.getLogger(getClass());
		private EpdStore store = Plugin.getEpdStore();

		public UploadAction() {
			setImageDescriptor(Icon.REFRESH.descriptor());
			setText(Messages.UploadDataSet);
		}

		@Override
		public void run() {
			EpdEditor editor = Editors.getActive();
			if (editor == null) {
				log.warn("called editor action but editor is not active");
				return;
			}
			ServerCredentials config = getCredentials();
			if (config == null) {
				log.error("Cannot upload data set -> no upload "
						+ "connection configured");
				return;
			}
			String question = Messages.UploadDataSet_Question + " "
					+ config.url + "?";
			boolean doIt = Question.ask(Messages.UploadDataSet + "?", question);
			if (doIt)
				uploadAndOpen(editor, config);
		}

		private ServerCredentials getCredentials() {
			if (store == null)
				return null;
			ServerConfig config = Configs.getServerConfig(store);
			if (config == null)
				return null;
			else
				return config.uploadCredentials;
		}

		private void uploadAndOpen(EpdEditor editor, ServerCredentials config) {
			EpdDataSet dataSet = editor.getDataSet();
			App.run("Upload data set",
					() -> upload(config, dataSet),
					() -> {
						Info.showPopup(Messages.DataSetUploaded,
								Messages.DataSetUploadedMessage);
					});
		}

		private void upload(ServerCredentials config, EpdDataSet dataSet) {
			try (Connection client = Connection.create(config)) {
				client.upload(dataSet, store);
			} catch (Exception e) {
				log.error("Data set upload failed", e);
			}
		}
	}
}
