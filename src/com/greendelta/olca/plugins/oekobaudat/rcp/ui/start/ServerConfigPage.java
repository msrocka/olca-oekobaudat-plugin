package com.greendelta.olca.plugins.oekobaudat.rcp.ui.start;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.openlca.app.util.UI;

import com.greendelta.olca.plugins.oekobaudat.io.ServerConfig;
import com.greendelta.olca.plugins.oekobaudat.rcp.Messages;

class ServerConfigPage extends WizardPage {

	private ServerConfig config;

	public ServerConfigPage(ServerConfig config) {
		super(Messages.ServerConfiguration);
		setTitle(Messages.ServerConfiguration);
		setDescription(Messages.ServerConfiguration_Description);
		this.config = config;
		setPageComplete(false);
	}

	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		UI.gridLayout(container, 1);
		ServerConfigGroup downloadGroup = new ServerConfigGroup(
				config.downloadCredentials);
		downloadGroup.render(container, Messages.SearchAndDownloads);
		ServerConfigGroup uploadGroup = new ServerConfigGroup(
				config.uploadCredentials);
		uploadGroup.render(container, Messages.Uploads);
		setControl(container);
		setPageComplete(true);
	}

}
