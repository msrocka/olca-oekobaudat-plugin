package com.greendelta.olca.plugins.oekobaudat.rcp.ui.search;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.ui.progress.UIJob;
import org.openlca.app.navigation.Navigator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.greendelta.olca.plugins.oekobaudat.io.EpdStore;
import com.greendelta.olca.plugins.oekobaudat.io.server.Connection;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDescriptor;
import com.greendelta.olca.plugins.oekobaudat.rcp.Plugin;
import com.greendelta.olca.plugins.oekobaudat.rcp.ui.editor.EpdEditor;
import com.greendelta.olca.plugins.oekobaudat.rcp.ui.start.StartPageView;

class DownloadJob extends UIJob {

	private Logger log = LoggerFactory.getLogger(getClass());

	private final EpdStore store = Plugin.getEpdStore();
	private final String json;
	private final Connection client;

	public DownloadJob(String json, Connection client) {
		super("Download data set");
		this.json = json;
		this.client = client;
		setUser(true);
	}

	@Override
	public IStatus runInUIThread(IProgressMonitor monitor) {
		try {
			monitor.beginTask("Download EPD data set", IProgressMonitor.UNKNOWN);
			EpdDescriptor descriptor = new Gson().fromJson(json,
					EpdDescriptor.class);
			BusyIndicator.showWhile(getDisplay(), () -> {
				client.download(descriptor, store);
			});
			StartPageView.refresh();
			EpdEditor.open(descriptor);
			Navigator.refresh();
			monitor.done();
			return Status.OK_STATUS;
		} catch (Exception e) {
			log.error("failed to download data set", e);
			return Status.CANCEL_STATUS;
		}
	}

}