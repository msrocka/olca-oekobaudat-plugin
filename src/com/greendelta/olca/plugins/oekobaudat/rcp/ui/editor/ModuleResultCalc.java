package com.greendelta.olca.plugins.oekobaudat.rcp.ui.editor;

import java.util.Objects;

import org.openlca.app.App;
import org.openlca.app.db.Cache;
import org.openlca.core.database.IDatabase;
import org.openlca.core.database.ImpactMethodDao;
import org.openlca.core.database.ProductSystemDao;
import org.openlca.core.math.CalculationSetup;
import org.openlca.core.math.SystemCalculator;
import org.openlca.core.model.ProductSystem;
import org.openlca.core.model.descriptors.ImpactCategoryDescriptor;
import org.openlca.core.model.descriptors.ImpactMethodDescriptor;
import org.openlca.core.results.SimpleResult;
import org.openlca.core.results.SimpleResultProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.greendelta.olca.plugins.oekobaudat.io.IndicatorMapping;
import com.greendelta.olca.plugins.oekobaudat.io.MappingConfig;
import com.greendelta.olca.plugins.oekobaudat.model.Amount;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDataSet;
import com.greendelta.olca.plugins.oekobaudat.model.Indicator;
import com.greendelta.olca.plugins.oekobaudat.model.IndicatorResult;
import com.greendelta.olca.plugins.oekobaudat.model.ModuleEntry;

public class ModuleResultCalc implements Runnable {

	private Logger log = LoggerFactory.getLogger(getClass());

	private final EpdDataSet dataSet;
	private final MappingConfig config;
	private final IDatabase database;

	public ModuleResultCalc(EpdDataSet dataSet, MappingConfig config,
			IDatabase database) {
		this.dataSet = dataSet;
		this.config = config;
		this.database = database;
	}

	@Override
	public void run() {
		new ModuleResultSync(dataSet).run();
		ImpactMethodDescriptor method = getMethod();
		if (method == null)
			return;
		for (ModuleEntry module : dataSet.moduleEntries) {
			if (module.productSystemId == null)
				continue;
			SimpleResultProvider<?> result = calculate(module, method);
			syncResults(module, result);
		}
	}

	private void syncResults(ModuleEntry module,
			SimpleResultProvider<?> result) {
		for (ImpactCategoryDescriptor impact : result.getImpactDescriptors()) {
			Indicator indicator = findIndicator(impact);
			if (indicator == null)
				continue;
			Amount amount = findAmount(indicator, module);
			if (amount == null)
				continue;
			amount.value = result.getTotalImpactResult(impact).value;
		}
	}

	private Indicator findIndicator(ImpactCategoryDescriptor impact) {
		for (IndicatorMapping mapping : config.indicatorMappings) {
			if (Objects.equals(impact.getRefId(), mapping.indicatorRefId))
				return mapping.indicator;
		}
		return null;
	}

	private Amount findAmount(Indicator indicator, ModuleEntry module) {
		IndicatorResult result = dataSet.getResult(indicator);
		if (result == null)
			return null;
		for (Amount amount : result.amounts) {
			if (Objects.equals(amount.module, module.module)
					&& Objects.equals(amount.scenario,
							module.scenario))
				return amount;
		}
		return null;
	}

	private ImpactMethodDescriptor getMethod() {
		String refId = config.impactMethodRefId;
		if (refId == null)
			return null;
		try {
			ImpactMethodDao dao = new ImpactMethodDao(database);
			for (ImpactMethodDescriptor descriptor : dao.getDescriptors()) {
				if (Objects.equals(descriptor.getRefId(), refId))
					return descriptor;
			}
			return null;
		} catch (Exception e) {
			log.error("failed to get LCIA method " + refId, e);
			return null;
		}
	}

	private SimpleResultProvider<?> calculate(ModuleEntry module,
			ImpactMethodDescriptor method) {
		try {
			ProductSystemDao dao = new ProductSystemDao(database);
			ProductSystem system = dao.getForRefId(module.productSystemId);
			CalculationSetup setup = new CalculationSetup(system);
			setup.impactMethod = method;
			// TODO: inject cache + solver
			SystemCalculator calculator = new SystemCalculator(
					Cache.getMatrixCache(), App.getSolver());
			SimpleResult result = calculator.calculateSimple(setup);
			SimpleResultProvider<?> provider = new SimpleResultProvider<>(
					result, Cache.getEntityCache());
			return provider;
		} catch (Exception e) {
			log.error("Calculation failed for module " + module, e);
			return null;
		}
	}

}
