package com.greendelta.olca.plugins.oekobaudat.rcp.ui;

import java.util.Objects;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

public class SimpleEditorInput implements IEditorInput {

	private String name;

	public SimpleEditorInput(String name) {
		this.name = name;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public Object getAdapter(Class adapter) {
		return null;
	}

	@Override
	public boolean exists() {
		return true;
	}

	@Override
	public ImageDescriptor getImageDescriptor() {
		return null;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public IPersistableElement getPersistable() {
		return null;
	}

	@Override
	public String getToolTipText() {
		return name;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj == this)
			return true;
		if (!(obj instanceof SimpleEditorInput))
			return false;
		SimpleEditorInput other = (SimpleEditorInput) obj;
		return Objects.equals(this.getName(), other.getName());
	}

}
