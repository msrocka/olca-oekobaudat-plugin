package com.greendelta.olca.plugins.oekobaudat.rcp.ui.search;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.ui.forms.editor.FormEditor;
import org.openlca.app.util.Editors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.greendelta.olca.plugins.oekobaudat.rcp.ui.SimpleEditorInput;

public class SearchView extends FormEditor {

	private static final String ID = "epd.SearchView";
	private Logger log = LoggerFactory.getLogger(getClass());

	public static void open() {
		Editors.open(new SimpleEditorInput("Online search"), ID);
	}

	@Override
	protected void addPages() {
		try {
			addPage(new SearchPage(this));
		} catch (Exception e) {
			log.error("failed to add page", e);
		}

	}

	@Override
	public void doSave(IProgressMonitor monitor) {
	}

	@Override
	public void doSaveAs() {
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

}
