package com.greendelta.olca.plugins.oekobaudat.rcp.ui.editor;

import java.text.SimpleDateFormat;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.openlca.app.components.TextDropComponent;
import org.openlca.app.editors.DataBinding;
import org.openlca.app.util.UI;
import org.openlca.app.util.UIFactory;
import org.openlca.core.model.ModelType;
import org.openlca.core.model.descriptors.ActorDescriptor;

import com.greendelta.olca.plugins.oekobaudat.model.AdminInfo;
import com.greendelta.olca.plugins.oekobaudat.rcp.Messages;

class AdminPage extends FormPage {

	private FormToolkit toolkit;

	private EpdEditor editor;
	private AdminInfo adminInfo;
	private DataBinding binding;

	public AdminPage(EpdEditor editor) {
		super(editor, "EpdInfoPage", Messages.AdministrativeInformation);
		this.editor = editor;
		binding = new DataBinding(editor);
		adminInfo = editor.getDataSet().adminInfo;
		if (adminInfo == null) {
			adminInfo = new AdminInfo();
			editor.getDataSet().adminInfo = adminInfo;
		}
	}

	@Override
	protected void createFormContent(IManagedForm managedForm) {
		toolkit = managedForm.getToolkit();
		ScrolledForm form = UI.formHeader(managedForm,
				Messages.AdministrativeInformation);
		Composite body = UI.formBody(form, managedForm.getToolkit());
		createDataEntrySection(body);
		createPublicationSection(body);
		form.reflow(true);
	}

	private void createDataEntrySection(Composite parent) {
		Composite composite = UI.formSection(parent, toolkit,
				Messages.DataEntry);
		createLastUpdateText(composite);
		TextDropComponent textDrop = UIFactory.createDropComponent(composite,
				Messages.Documentor, toolkit, ModelType.ACTOR);
		textDrop.setContent(adminInfo.documentor);
		textDrop.setHandler(d -> {
			if (!(d instanceof ActorDescriptor))
				adminInfo.documentor = null;
			else
				adminInfo.documentor = (ActorDescriptor) d;
			editor.setDirty(true);
		});
	}

	private void createLastUpdateText(Composite composite) {
		final Text text = UI.formText(composite, toolkit, Messages.LastUpdate);
		final SimpleDateFormat format = new SimpleDateFormat(
				"yyyy-MM-dd'T'HH:mm:ssZ");
		text.setEditable(false);
		editor.onSaved(() -> {
			if (adminInfo.lastUpdate != null)
				text.setText(format.format(adminInfo.lastUpdate));
		});
		if (adminInfo.lastUpdate == null)
			return;
		text.setText(format.format(adminInfo.lastUpdate));
	}

	private void createPublicationSection(Composite parent) {
		Composite composite = UI.formSection(parent, toolkit,
				Messages.PublicationAndOwnership);
		createVersionText(composite);
		TextDropComponent textDrop = UIFactory.createDropComponent(composite,
				Messages.Owner, toolkit, ModelType.ACTOR);
		textDrop.setContent(adminInfo.owner);
		textDrop.setHandler((descriptor) -> {
			if (!(descriptor instanceof ActorDescriptor))
				adminInfo.owner = null;
			else
				adminInfo.owner = (ActorDescriptor) descriptor;
			editor.setDirty(true);
		});
		Button copyrightCheck = UI
				.formCheckBox(composite, toolkit, Messages.Copyright);
		binding.onBoolean(() -> adminInfo, "copyright", copyrightCheck);
		Text restrictionsText = UI.formMultiText(composite, toolkit,
				Messages.AccessRestrictions);
		binding.onString(() -> adminInfo, "accessRestrictions",
				restrictionsText);
	}

	private void createVersionText(Composite container) {
		VersionField v = new VersionField(container, toolkit);
		v.setVersion(adminInfo.version);
		editor.onSaved(() -> v.setVersion(adminInfo.version));
		v.onChange((version) -> {
			adminInfo.version = version;
			editor.setDirty(true);
		});
	}
}
