package com.greendelta.olca.plugins.oekobaudat.rcp.ui.start;

import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.progress.UIJob;
import org.openlca.app.App;
import org.openlca.app.rcp.html.HtmlFolder;
import org.openlca.app.rcp.html.WebPage;
import org.openlca.app.util.Desktop;
import org.openlca.app.util.InformationPopup;
import org.openlca.app.util.Question;
import org.openlca.app.util.UI;
import org.openlca.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.greendelta.olca.plugins.oekobaudat.io.EpdStore;
import com.greendelta.olca.plugins.oekobaudat.io.server.Connection;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDescriptor;
import com.greendelta.olca.plugins.oekobaudat.rcp.Messages;
import com.greendelta.olca.plugins.oekobaudat.rcp.Plugin;
import com.greendelta.olca.plugins.oekobaudat.rcp.ui.editor.EpdEditor;
import com.greendelta.olca.plugins.oekobaudat.rcp.ui.mappings.IndicatorMappingEditor;
import com.greendelta.olca.plugins.oekobaudat.rcp.ui.properties.MaterialPropertyEditor;
import com.greendelta.olca.plugins.oekobaudat.rcp.ui.search.SearchView;

import javafx.scene.web.WebEngine;
import netscape.javascript.JSObject;

class StartPage extends FormPage implements WebPage {

	private Logger log = LoggerFactory.getLogger(getClass());

	private Connection client;
	private WebEngine webkit;
	private EpdStore store = Plugin.getEpdStore();

	public StartPage(StartPageView view) {
		super(view, "StartPage", "Home");
	}

	@Override
	public String getUrl() {
		return HtmlFolder.getUrl(Plugin.getDefault().getBundle(),
				"start_page_view.html");
	}

	@Override
	protected void createFormContent(IManagedForm managedForm) {
		FormToolkit toolkit = managedForm.getToolkit();
		Composite body = UI.formBody(managedForm.getForm(), toolkit);
		body.setLayout(new FillLayout());
		UI.createWebView(body, this);
	}

	@Override
	public void onLoaded(WebEngine webkit) {
		this.webkit = webkit;
		try {
			JSObject window = (JSObject) webkit.executeScript("window");
			window.setMember("java", new JsHandler());
			String command = "setMessages(" + Messages.asJson() + ")";
			webkit.executeScript(command);
			loadDescriptors();
		} catch (Exception e) {
			log.error("failed to set start page data", e);
		}
	}

	void loadDescriptors() {
		try {
			List<EpdDescriptor> descriptors = store.getDescriptors();
			descriptors.sort((d1, d2) -> Strings.compare(d1.name, d2.name));
			String json = new Gson().toJson(descriptors);
			String command = "setData(" + json + ")";
			webkit.executeScript(command);
			webkit.executeScript(
					"if (!document.getElementById('FirebugLite')){E = document['createElement' + 'NS'] && document.documentElement.namespaceURI;E = E ? document['createElement' + 'NS'](E, 'script') : document['createElement']('script');E['setAttribute']('id', 'FirebugLite');E['setAttribute']('src', 'https://getfirebug.com/' + 'firebug-lite.js' + '#startOpened');E['setAttribute']('FirebugLite', '4');(document['getElementsByTagName']('head')[0] || document['getElementsByTagName']('body')[0]).appendChild(E);E = new Image;E['setAttribute']('src', 'https://getfirebug.com/' + '#startOpened');}");
		} catch (Exception e) {
			log.error("failed to set descriptors in start page", e);
		}
	}

	@Override
	public void dispose() {
		try {
			if (client != null) {
				log.trace("close network client");
				client.close();
			}
		} catch (Exception e) {
			log.error("failed to close network client", e);
		}
		super.dispose();
	}

	public class JsHandler {

		public void createEpd() {
			EpdWizard.open();
		}

		public void openServerConfiguration() {
			ServerConfigWizard.open();
		}

		public void openSearchPage() {
			SearchView.open();
		}

		public void openIndicatorMapping() {
			IndicatorMappingEditor.open();
		}

		public void openPropertiesEditor() {
			MaterialPropertyEditor.open();
		}

		public void doOpen(String descriptorJson) {
			EpdDescriptor descriptor = getDescriptor(descriptorJson);
			EpdEditor.open(descriptor);
		}

		public void doDelete(String descriptorJson) {
			EpdDescriptor descriptor = getDescriptor(descriptorJson);
			boolean doIt = Question.ask("Delete EPD?",
					"Do you really want to delete the EPD '"
							+ descriptor.name + "'?");
			if (!doIt)
				return;
			new DeleteJob(descriptor).schedule();
		}

		private EpdDescriptor getDescriptor(String json) {
			try {
				return new Gson().fromJson(json, EpdDescriptor.class);
			} catch (Exception e) {
				log.error("failed to parse EPD descriptor " + json, e);
				return null;
			}
		}

		public void openUrl(String url) {
			App.run("open " + url, () -> Desktop.browse(url));
		}
	}

	private class DeleteJob extends UIJob {

		private EpdDescriptor descriptor;

		public DeleteJob(EpdDescriptor descriptor) {
			super("Delete EPD " + descriptor.name);
			this.descriptor = descriptor;
		}

		@Override
		public IStatus runInUIThread(IProgressMonitor monitor) {
			try {
				Plugin.getEpdStore().delete(descriptor);
				loadDescriptors();
				InformationPopup.show("Deleted",
						"Deleted EPD " + descriptor.name);
				return Status.OK_STATUS;
			} catch (Exception e) {
				log.error("failed to delete EPD " + descriptor.name, e);
				return Status.CANCEL_STATUS;
			}
		}
	}
}
