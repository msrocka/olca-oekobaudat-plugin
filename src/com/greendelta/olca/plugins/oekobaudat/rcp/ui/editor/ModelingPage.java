package com.greendelta.olca.plugins.oekobaudat.rcp.ui.editor;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.openlca.app.editors.DataBinding;
import org.openlca.app.util.UI;
import org.openlca.app.util.viewers.Viewers;

import com.greendelta.olca.plugins.oekobaudat.model.EpdDataSet;
import com.greendelta.olca.plugins.oekobaudat.model.ModellingInfo;
import com.greendelta.olca.plugins.oekobaudat.model.SubType;
import com.greendelta.olca.plugins.oekobaudat.rcp.Labels;
import com.greendelta.olca.plugins.oekobaudat.rcp.Messages;

class ModelingPage extends FormPage {

	private FormToolkit toolkit;

	private EpdEditor editor;
	private ModellingInfo modellingInfo;

	private DataBinding binding;

	public ModelingPage(EpdEditor editor) {
		super(editor, "EpdInfoPage", Messages.ModellingAndValidation);
		this.editor = editor;
		binding = new DataBinding(editor);
		EpdDataSet dataSet = editor.getDataSet();
		modellingInfo = dataSet.modellingInfo;
		if (modellingInfo == null) {
			modellingInfo = new ModellingInfo();
			dataSet.modellingInfo = modellingInfo;
		}
	}

	@Override
	protected void createFormContent(IManagedForm managedForm) {
		toolkit = managedForm.getToolkit();
		ScrolledForm form = UI.formHeader(managedForm,
				Messages.ModellingAndValidation);
		Composite body = UI.formBody(form, managedForm.getToolkit());
		createModelingSection(body);
		SourceTable.createFor(modellingInfo.methodDetails)
				.withEditor(editor)
				.withTitle(Messages.LCAMethodDetails)
				.render(body, toolkit);
		SourceTable.createFor(modellingInfo.sources)
				.withEditor(editor)
				.withTitle(Messages.DataSources)
				.render(body, toolkit);
		new ReviewSection(modellingInfo, editor)
				.render(body, toolkit, form);
		form.reflow(true);
	}

	private void createModelingSection(Composite parent) {
		Composite composite = UI.formSection(parent, toolkit,
				Messages.ModellingAndValidation);
		UI.formLabel(composite, toolkit, Messages.Subtype);
		createSubTypeViewer(composite);
		Text useAdviceText = UI.formMultiText(composite, toolkit,
				Messages.UseAdvice);
		binding.onString(() -> modellingInfo, "useAdvice", useAdviceText);
	}

	private ComboViewer createSubTypeViewer(Composite parent) {
		ComboViewer viewer = new ComboViewer(parent, SWT.READ_ONLY);
		UI.gridData(viewer.getControl(), true, false);
		viewer.setContentProvider(ArrayContentProvider.getInstance());
		viewer.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof SubType) {
					SubType subType = (SubType) element;
					return Labels.getEnumText(subType);
				}
				return super.getText(element);
			}
		});
		viewer.setInput(SubType.values());
		selectSubType(viewer);
		viewer.addSelectionChangedListener((event) -> {
			SubType type = Viewers.getFirst(event.getSelection());
			modellingInfo.subType = type;
			editor.setDirty(true);
		});
		return viewer;
	}

	private void selectSubType(ComboViewer viewer) {
		if (modellingInfo.subType == null)
			return;
		StructuredSelection selection = new StructuredSelection(
				modellingInfo.subType);
		viewer.setSelection(selection);
	}
}
