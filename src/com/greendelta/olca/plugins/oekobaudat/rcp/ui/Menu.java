package com.greendelta.olca.plugins.oekobaudat.rcp.ui;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.ui.menus.ExtensionContributionFactory;
import org.eclipse.ui.menus.IContributionRoot;
import org.eclipse.ui.services.IServiceLocator;
import org.openlca.app.M;
import org.openlca.app.db.Database;
import org.openlca.app.util.Actions;
import org.openlca.app.util.Warning;
import org.openlca.core.database.IDatabase;

import com.greendelta.olca.plugins.oekobaudat.rcp.Messages;
import com.greendelta.olca.plugins.oekobaudat.rcp.ui.search.SearchView;
import com.greendelta.olca.plugins.oekobaudat.rcp.ui.start.EpdWizard;
import com.greendelta.olca.plugins.oekobaudat.rcp.ui.start.StartPageView;

public class Menu extends ExtensionContributionFactory {

	@Override
	public void createContributionItems(IServiceLocator serviceLocator,
			IContributionRoot additions) {
		MenuManager menu = new MenuManager(Messages.EPDEditor);
		menu.add(Actions.create(Messages.StartPage, null, this::startPage));
		menu.add(Actions.create(Messages.NewEPD, null, this::newEPD));
		menu.add(Actions.create(Messages.SearchEPDs, null, this::searchEPDs));
		additions.addContributionItem(menu, null);
	}

	private void startPage() {
		if (dbOpen())
			StartPageView.open();
	}

	private void newEPD() {
		if (dbOpen())
			EpdWizard.open();
	}

	private void searchEPDs() {
		if (dbOpen())
			SearchView.open();
	}

	private boolean dbOpen() {
		IDatabase database = Database.get();
		if (database != null)
			return true;
		Warning.showBox(M.NoDatabaseOpened, M.NeedOpenDatabase);
		return false;
	}

}
