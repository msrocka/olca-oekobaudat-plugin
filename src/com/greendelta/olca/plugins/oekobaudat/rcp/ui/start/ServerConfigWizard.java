package com.greendelta.olca.plugins.oekobaudat.rcp.ui.start;

import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.openlca.app.util.Error;
import org.openlca.app.util.UI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.greendelta.olca.plugins.oekobaudat.io.Configs;
import com.greendelta.olca.plugins.oekobaudat.io.EpdStore;
import com.greendelta.olca.plugins.oekobaudat.io.ServerConfig;
import com.greendelta.olca.plugins.oekobaudat.io.ServerCredentials;
import com.greendelta.olca.plugins.oekobaudat.rcp.Plugin;

public class ServerConfigWizard extends Wizard {

	private Logger log = LoggerFactory.getLogger(getClass());

	private final ServerConfig config;
	private ServerConfigPage page;

	public static void open() {
		EpdStore store = Plugin.getEpdStore();
		if (store == null) {
			Error.showBox("No active database",
					"Please activate a database to create an EPD");
			return;
		}
		ServerConfig config = getConfig(store);
		WizardDialog wizardDialog = new WizardDialog(UI.shell(),
				new ServerConfigWizard(config));
		wizardDialog.open();
	}

	private static ServerConfig getConfig(EpdStore store) {
		ServerConfig config = Configs.getServerConfig(store);
		if (config == null)
			config = new ServerConfig();
		if (config.uploadCredentials == null)
			config.uploadCredentials = new ServerCredentials();
		if (config.downloadCredentials == null)
			config.downloadCredentials = new ServerCredentials();
		return config;
	}

	private ServerConfigWizard(ServerConfig config) {
		this.config = config;
	}

	@Override
	public void addPages() {
		page = new ServerConfigPage(config);
		addPage(page);
	}

	@Override
	public boolean performFinish() {
		log.trace("save server connection data");
		try {
			Configs.save(config, Plugin.getEpdStore());
			return true;
		} catch (Exception e) {
			log.error("failed to save server configuration");
			return false;
		}
	}
}
