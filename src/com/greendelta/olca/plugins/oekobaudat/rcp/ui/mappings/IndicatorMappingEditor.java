package com.greendelta.olca.plugins.oekobaudat.rcp.ui.mappings;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.forms.editor.FormEditor;
import org.openlca.app.editors.IEditor;
import org.openlca.app.util.Editors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.greendelta.olca.plugins.oekobaudat.io.Configs;
import com.greendelta.olca.plugins.oekobaudat.io.EpdStore;
import com.greendelta.olca.plugins.oekobaudat.io.MappingConfig;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDataSet;
import com.greendelta.olca.plugins.oekobaudat.rcp.Plugin;
import com.greendelta.olca.plugins.oekobaudat.rcp.ui.SimpleEditorInput;

public class IndicatorMappingEditor extends FormEditor implements IEditor {

	private static final String ID = "epd.indicator_mapping";

	private Logger log = LoggerFactory.getLogger(getClass());

	private IndicatorMappingPage mappingPage;
	private EpdDataSet dataSet;
	private boolean dirty;
	private MappingConfig config;

	public static void open() {
		Editors.open(new SimpleEditorInput("Indicator Mapping"), ID);
	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		super.init(site, input);
		try {
			EpdStore store = Plugin.getEpdStore();
			config = Configs.getMappingConfig(store);
		} catch (Exception e) {
			log.error("failed to get mapping config", e);
			config = new MappingConfig();
		}
	}

	public EpdDataSet getDataSet() {
		return dataSet;
	}

	@Override
	public void setDirty(boolean b) {
		if (dirty != b) {
			dirty = b;
			editorDirtyStateChanged();
		}
	}

	@Override
	public boolean isDirty() {
		return dirty;
	}

	@Override
	protected void addPages() {
		try {
			mappingPage = new IndicatorMappingPage(this, config);
			addPage(mappingPage);
		} catch (Exception e) {
			log.error("failed to add editor page", e);
		}
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		try {
			EpdStore store = Plugin.getEpdStore();
			Configs.save(config, store);
			setDirty(false);
		} catch (Exception e) {
			log.error("failed to save mapping config", e);
		}
	}

	@Override
	public void doSaveAs() {
		setDirty(false);
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

}
