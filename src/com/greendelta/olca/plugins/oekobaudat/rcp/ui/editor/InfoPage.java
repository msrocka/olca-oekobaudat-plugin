package com.greendelta.olca.plugins.oekobaudat.rcp.ui.editor;

import java.util.List;
import java.util.Objects;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.events.HyperlinkAdapter;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ImageHyperlink;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.openlca.app.components.TextDropComponent;
import org.openlca.app.db.Database;
import org.openlca.app.editors.DataBinding;
import org.openlca.app.rcp.images.Images;
import org.openlca.app.util.Colors;
import org.openlca.app.util.FileType;
import org.openlca.app.util.UI;
import org.openlca.app.util.UIFactory;
import org.openlca.app.viewers.combo.LocationViewer;
import org.openlca.core.database.LocationDao;
import org.openlca.core.model.Location;
import org.openlca.core.model.ModelType;
import org.openlca.core.model.descriptors.SourceDescriptor;
import org.openlca.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.greendelta.olca.plugins.oekobaudat.model.DataSetInfo;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDataSet;
import com.greendelta.olca.plugins.oekobaudat.model.SafetyMargins;
import com.greendelta.olca.plugins.oekobaudat.rcp.Browser;
import com.greendelta.olca.plugins.oekobaudat.rcp.Messages;
import com.greendelta.olca.plugins.oekobaudat.rcp.Plugin;

class InfoPage extends FormPage {

	private Logger log = LoggerFactory.getLogger(getClass());

	private EpdEditor editor;
	private FormToolkit toolkit;

	private EpdDataSet dataSet;
	private DataSetInfo info;
	private DataBinding binding;

	public InfoPage(EpdEditor editor) {
		super(editor, "EpdInfoPage", Messages.DataSetInformation);
		this.editor = editor;
		dataSet = editor.getDataSet();
		info = dataSet.dataSetInfo;
		if (info == null) {
			info = new DataSetInfo();
			dataSet.dataSetInfo = info;
		}
		binding = new DataBinding(editor);
	}

	@Override
	protected void createFormContent(IManagedForm managedForm) {
		toolkit = managedForm.getToolkit();
		ScrolledForm form = UI.formHeader(managedForm,
				Messages.EPD + ": " + info.baseName);
		Composite body = UI.formBody(form, managedForm.getToolkit());
		createInfoSection(body);
		new CategorySection(editor, info).render(body, toolkit);
		SourceTable.createFor(info.externalDocs)
				.withEditor(editor)
				.withTitle(Messages.ExternalDocumentationSources)
				.render(body, toolkit);
		createSafetyMarginsSection(body);
		createTimeSection(body);
		createGeographySection(body);
		createTechnologySection(body);
		SourceTable.createFor(info.pictures)
				.withEditor(editor)
				.withTitle(Messages.FlowDiagramsOrPictures)
				.render(body, toolkit);
		form.reflow(true);
	}

	private void createInfoSection(Composite parent) {
		Composite composite = UI.formSection(parent, toolkit,
				Messages.GeneralInformation);
		createText(composite, "uuid", Messages.UUID).setEditable(false);
		createText(composite, "baseName", Messages.Name);
		createText(composite, "quantitativeProperties",
				Messages.QuantitativeProperties);
		createText(composite, "synonyms", Messages.Synonyms);
		Text generalComment = UI.formMultiText(composite, Messages.Comment);
		binding.onString(() -> info, "description", generalComment);
		createFileLink(composite);
	}

	private void createFileLink(Composite composite) {
		UI.formLabel(composite, toolkit, Messages.File);
		ImageHyperlink link = toolkit.createImageHyperlink(composite, SWT.NONE);
		link.setForeground(Colors.linkBlue());
		link.setImage(Images.get(FileType.XML));
		link.setText("../processes/" + info.uuid + ".xml");
		link.addHyperlinkListener(new HyperlinkAdapter() {
			@Override
			public void linkActivated(HyperlinkEvent e) {
				Browser.openFile(dataSet, Plugin.getEpdStore());
			}
		});
	}

	private void createSafetyMarginsSection(Composite parent) {
		Composite composite = UI.formSection(parent, toolkit,
				Messages.SafetyMargins);
		if (info.safetyMargins == null)
			info.safetyMargins = new SafetyMargins();
		Text marginsText = UI.formText(composite, Messages.SafetyMargin);
		if (info.safetyMargins.margins != null) {
			marginsText.setText(info.safetyMargins.margins.toString());
		}
		marginsText.addModifyListener((e) -> modifyMargins(marginsText));
		Text text = UI.formMultiText(composite, toolkit, Messages.Description);
		binding.onString(() -> info.safetyMargins, "description", text);
	}

	private void modifyMargins(Text text) {
		String t = text.getText();
		SafetyMargins margins = info.safetyMargins;
		if (Strings.nullOrEmpty(t)) {
			margins.margins = null;
			editor.setDirty(true);
			return;
		}
		try {
			margins.margins = Double.parseDouble(t);
		} catch (Exception e) {
			if (margins.margins != null)
				text.setText(margins.margins.toString());
			else
				text.setText("");
		}
		editor.setDirty(true);
	}

	private Text createText(Composite composite, String property, String label) {
		Text text = UI.formText(composite, toolkit, label);
		if (property != null)
			binding.onString(() -> info, property, text);
		return text;
	}

	private void createTechnologySection(Composite body) {
		Composite composite = UI
				.formSection(body, toolkit, Messages.Technology);
		Text text = UI.formMultiText(composite, toolkit,
				Messages.TechnologyDescription);
		binding.onString(() -> info, "technology", text);
		Text appText = UI.formMultiText(composite, toolkit,
				Messages.TechnologicalApplicability);
		binding.onString(() -> info, "technologicalApplicability", appText);
		TextDropComponent drop = UIFactory.createDropComponent(composite,
				Messages.Pictogram, toolkit, ModelType.SOURCE);
		drop.setContent(info.pictogram);
		drop.setHandler((d) -> {
			if (!(d instanceof SourceDescriptor))
				info.pictogram = null;
			else
				info.pictogram = (SourceDescriptor) d;
			editor.setDirty(true);
		});
	}

	private void createTimeSection(Composite body) {
		Composite composite = UI.formSection(body, toolkit, Messages.Time);
		Text refYearText = UI.formText(composite, toolkit,
				Messages.ReferenceYear);
		binding.onInt(() -> info, "referenceYear", refYearText);
		Text validUntilText = UI.formText(composite, toolkit,
				Messages.ValidUntil);
		binding.onInt(() -> info, "validUntil", validUntilText);
		Text commentText = UI.formMultiText(composite, toolkit,
				Messages.TimeDescription);
		binding.onString(() -> info, "timeComment", commentText);
	}

	private void createGeographySection(Composite body) {
		Composite composite = UI.formSection(body, toolkit,
				Messages.Geography);
		toolkit.createLabel(composite, Messages.Location);
		LocationViewer viewer = new LocationViewer(composite);
		viewer.setNullable(true);
		initLocationViewer(viewer);
		viewer.addSelectionChangedListener((location) -> {
			if (location == null)
				info.locationCode = null;
			else
				info.locationCode = location.getCode();
			editor.setDirty(true);
		});
		Text text = UI.formMultiText(composite, toolkit,
				Messages.GeographyDescription);
		binding.onString(() -> info, "geographyComment", text);
	}

	private void initLocationViewer(LocationViewer viewer) {
		try {
			LocationDao dao = new LocationDao(Database.get());
			List<Location> locations = dao.getAll();
			viewer.setInput(locations);
			String code = info.locationCode;
			if (code == null)
				return;
			for (Location location : locations) {
				if (Objects.equals(location.getCode(), code)) {
					viewer.select(location);
					break;
				}
			}
		} catch (Exception e) {
			log.error("failed to initialize location viewer", e);
		}
	}

}