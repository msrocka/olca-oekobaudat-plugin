package com.greendelta.olca.plugins.oekobaudat.rcp.ui.mappings;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.openlca.app.util.tables.Tables;
import org.openlca.app.viewers.table.AbstractTableViewer;
import org.openlca.app.viewers.table.modify.ComboBoxCellModifier;
import org.openlca.core.model.descriptors.ImpactCategoryDescriptor;
import org.openlca.util.Strings;

import com.greendelta.olca.plugins.oekobaudat.io.IndicatorMapping;
import com.greendelta.olca.plugins.oekobaudat.rcp.Labels;
import com.greendelta.olca.plugins.oekobaudat.rcp.Messages;

public class IndicatorViewer extends AbstractTableViewer<IndicatorMapping> {

	private final static String INDICATOR = Messages.Indicator;
	private final static String CATEGORY = Messages.Category;
	private final static String UNIT = Messages.Unit;

	private IndicatorMappingEditor editor;
	private ImpactCategoryDescriptor[] categories = new ImpactCategoryDescriptor[0];

	public IndicatorViewer(Composite parent, IndicatorMappingEditor editor,
			List<IndicatorMapping> mappings) {
		super(parent);
		this.editor = editor;
		getModifySupport().bind(CATEGORY, new CategoryModifier());
		Tables.bindColumnWidths(getViewer(), 0.50, 0.25, 0.25);
		getViewer().refresh(true);
		initialInput(mappings);
	}

	private void initialInput(List<IndicatorMapping> mappings) {
		Collections.sort(mappings, (o1, o2) -> {
			String i1 = Labels.epdIndicator(o1.indicator);
			String i2 = Labels.epdIndicator(o2.indicator);
			return Strings.compare(i1, i2);
		});
		setInput(mappings.toArray(new IndicatorMapping[mappings.size()]));
	}

	public void refresh() {
		getViewer().refresh(true);
	}

	public void setCategories(List<ImpactCategoryDescriptor> categories) {
		if (categories == null)
			this.categories = new ImpactCategoryDescriptor[0];
		Collections.sort(categories, (o1, o2) ->
				Strings.compare(o1.getName(), o2.getName()));
		this.categories = categories
				.toArray(new ImpactCategoryDescriptor[categories.size()]);
	}

	@Override
	protected String[] getColumnHeaders() {
		return new String[] { INDICATOR, CATEGORY, UNIT };
	}

	@Override
	protected IBaseLabelProvider getLabelProvider() {
		return new LabelProvider();
	}

	private class LabelProvider extends BaseLabelProvider implements
			ITableLabelProvider {

		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			return null;
		}

		@Override
		public String getColumnText(Object element, int col) {
			if (!(element instanceof IndicatorMapping))
				return null;
			IndicatorMapping mapping = (IndicatorMapping) element;
			switch (col) {
			case 0:
				return Labels.getEnumText(mapping.indicator);
			case 1:
				return mapping.indicatorLabel;
			case 2:
				return mapping.unitLabel;
			}
			return null;
		}
	}

	private class CategoryModifier extends
			ComboBoxCellModifier<IndicatorMapping, ImpactCategoryDescriptor> {

		@Override
		protected ImpactCategoryDescriptor getItem(IndicatorMapping element) {
			String refId = element.indicatorRefId;
			if (refId == null || categories == null)
				return null;
			for (ImpactCategoryDescriptor descriptor : categories) {
				if (Objects.equals(refId, descriptor.getRefId()))
					return descriptor;
			}
			return null;
		}

		@Override
		protected ImpactCategoryDescriptor[] getItems(IndicatorMapping element) {
			return categories;
		}

		@Override
		protected String getText(ImpactCategoryDescriptor category) {
			if (category == null)
				return "";
			return category.getName();
		}

		@Override
		protected void setItem(IndicatorMapping element,
				ImpactCategoryDescriptor item) {
			if (element == null)
				return;
			if (item == null) {
				element.indicatorLabel = null;
				element.indicatorRefId = null;
			} else {
				element.indicatorLabel = item.getName();
				element.indicatorRefId = item.getRefId();
			}
			editor.setDirty(true);
		}
	}

}
