package com.greendelta.olca.plugins.oekobaudat.rcp.ui.editor;

import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.openlca.app.util.UI;

public class ScenarioWizard extends Wizard {

	protected ScenarioWizardPage page;

	public static void open() {
		WizardDialog wizardDialog = new WizardDialog(UI.shell(),
				new ScenarioWizard());
		wizardDialog.open();
	}

	@Override
	public void addPages() {
		page = new ScenarioWizardPage();
		addPage(page);
	}

	@Override
	public boolean performFinish() {
		return true;
	}

}
