package com.greendelta.olca.plugins.oekobaudat.rcp.ui.editor;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.openlca.app.App;
import org.openlca.app.components.FileChooser;
import org.openlca.app.db.Database;
import org.openlca.app.rcp.images.Icon;
import org.openlca.app.rcp.images.Images;
import org.openlca.app.util.Actions;
import org.openlca.app.util.UI;
import org.openlca.app.util.tables.Tables;
import org.openlca.app.util.viewers.Viewers;
import org.openlca.app.viewers.table.modify.ComboBoxCellModifier;
import org.openlca.app.viewers.table.modify.ModifySupport;
import org.openlca.app.viewers.table.modify.TextCellModifier;
import org.openlca.core.database.IDatabase;
import org.openlca.core.database.ProductSystemDao;
import org.openlca.core.model.ModelType;
import org.openlca.core.model.descriptors.ProductSystemDescriptor;
import org.openlca.util.Strings;

import com.greendelta.olca.plugins.oekobaudat.io.Configs;
import com.greendelta.olca.plugins.oekobaudat.io.EpdStore;
import com.greendelta.olca.plugins.oekobaudat.io.MappingConfig;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDataSet;
import com.greendelta.olca.plugins.oekobaudat.model.Module;
import com.greendelta.olca.plugins.oekobaudat.model.ModuleEntry;
import com.greendelta.olca.plugins.oekobaudat.model.Scenario;
import com.greendelta.olca.plugins.oekobaudat.rcp.Labels;
import com.greendelta.olca.plugins.oekobaudat.rcp.Messages;
import com.greendelta.olca.plugins.oekobaudat.rcp.Plugin;

class ModulePage extends FormPage {

	private EpdEditor editor;
	private FormToolkit toolkit;

	private List<ModuleEntry> modules;
	private EpdDataSet dataSet;
	private ScenarioTable scenarioTable;
	private TableViewer moduleTable;
	private ModuleResultTable resultTable;

	public ModulePage(EpdEditor editor) {
		super(editor, "ModulesPage", Messages.EnvironmentalIndicators);
		this.editor = editor;
		dataSet = editor.getDataSet();
		modules = dataSet.moduleEntries;
		Collections.sort(modules, (e1, e2) -> {
			Module m1 = e1.module;
			Module m2 = e2.module;
			if (m1 == null || m2 == null || m1 == m2)
				return Strings.compare(e1.scenario, e2.scenario);
			return Strings.compare(m1.getLabel(), m2.getLabel());
		});
	}

	@Override
	protected void createFormContent(IManagedForm managedForm) {
		toolkit = managedForm.getToolkit();
		ScrolledForm form = UI.formHeader(managedForm,
				Messages.EnvironmentalIndicators);
		Composite body = UI.formBody(form, managedForm.getToolkit());
		createScenarioSection(body);
		moduleTable = createModuleSection(body);
		moduleTable.setInput(modules);
		resultTable = createResultSection(body);
		resultTable.refresh();
		form.reflow(true);
	}

	private void createScenarioSection(Composite parent) {
		Section section = UI.section(parent, toolkit, Messages.Scenarios);
		section.setExpanded(false);
		UI.gridData(section, true, false);
		Composite composite = UI.sectionClient(section, toolkit);
		UI.gridLayout(composite, 1);
		scenarioTable = new ScenarioTable(editor, composite);
		scenarioTable.setInput();
		scenarioTable.bindTo(section);
	}

	private TableViewer createModuleSection(Composite parent) {
		Section section = UI.section(parent, toolkit, Messages.Modules);
		Composite composite = UI.sectionClient(section, toolkit);
		UI.gridLayout(composite, 1);
		String[] columns = new String[] { Messages.Module, Messages.Scenario,
				Messages.ProductSystem, Messages.Description };
		TableViewer viewer = Tables.createViewer(composite, columns);
		ModuleLabel label = new ModuleLabel();
		viewer.setLabelProvider(label);
		Viewers.sortByLabels(viewer, label, 0, 1, 2, 3);
		Tables.bindColumnWidths(viewer, 0.25, 0.25, 0.25, 0.25);
		Action[] actions = createModuleActions();
		Actions.bind(section, actions);
		Actions.bind(viewer, actions);
		ModifySupport<ModuleEntry> modifiers = new ModifySupport<>(viewer);
		modifiers.bind(Messages.Module, new ModuleModifier());
		modifiers.bind(Messages.Scenario, new ScenarioModifier());
		modifiers.bind(Messages.ProductSystem,
				new ProductSystemCellEditor(viewer.getTable(), editor));
		modifiers.bind(Messages.Description, new DescriptionModifier());
		return viewer;
	}

	private Action[] createModuleActions() {
		Action[] actions = new Action[2];
		actions[0] = Actions.onAdd(() -> createModule());
		actions[1] = Actions.onRemove(() -> removeModule());
		return actions;
	}

	private void createModule() {
		ModuleEntry module = new ModuleEntry();
		module.module = Module.A1;
		modules.add(module);
		moduleTable.setInput(modules);
		editor.setDirty(true);
	}

	private void removeModule() {
		ModuleEntry module = Viewers.getFirstSelected(moduleTable);
		if (module == null)
			return;
		modules.remove(module);
		moduleTable.setInput(modules);
		editor.setDirty(true);
	}

	private ModuleResultTable createResultSection(Composite body) {
		Section section = UI.section(body, toolkit, Messages.Results);
		UI.gridData(section, true, true);
		Composite composite = UI.sectionClient(section, toolkit);
		UI.gridLayout(composite, 1);
		ModuleResultTable table = new ModuleResultTable(editor, dataSet);
		table.create(composite);
		Actions.bind(section, createResultActions());
		return table;
	}

	private Action[] createResultActions() {
		Action[] actions = new Action[4];
		actions[0] = Actions.onCalculate(() -> calculateResults());
		actions[1] = Actions.create(Messages.SynchronizeWithModules,
				Icon.CHECK_TRUE.descriptor(), () -> {
					new ModuleResultSync(dataSet).run();
					resultTable.refresh();
					editor.setDirty(true);
				});
		actions[2] = Actions.create(Messages.Export, Icon.EXPORT.descriptor(),
				() -> exportResults());
		actions[3] = Actions.create(Messages.Import, Icon.IMPORT.descriptor(),
				() -> importResults());
		return actions;
	}

	private void exportResults() {
		File file = FileChooser.forExport("*.xlsx",
				"results.xlsx");
		if (file == null)
			return;
		ModuleResultExport export = new ModuleResultExport(dataSet, file);
		App.run(Messages.Export, export, () -> {
			if (export.isDoneWithSuccess())
				return;
			String message = "@Export failed. Is the file already opened?";
			org.openlca.app.util.Error.showBox("@Export failed", message);
		});
	}

	private void importResults() {
		File file = FileChooser.forImport("*.xlsx");
		if (file == null)
			return;
		ModuleResultImport resultImport = new ModuleResultImport(dataSet, file);
		App.run(Messages.Import, resultImport, () -> {
			resultTable.refresh();
			moduleTable.refresh();
			scenarioTable.setInput();
			editor.setDirty(true);
		});
	}

	private void calculateResults() {
		EpdStore store = Plugin.getEpdStore();
		MappingConfig config = Configs.getMappingConfig(store);
		IDatabase database = Database.get();
		ModuleResultCalc calc = new ModuleResultCalc(dataSet, config, database);
		App.run(Messages.CalculateResults, calc, () -> {
			resultTable.refresh();
			editor.setDirty(true);
		});
	}

	private class ModuleLabel extends LabelProvider implements
			ITableLabelProvider {

		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			if (columnIndex == 2)
				return Images.get(ModelType.PRODUCT_SYSTEM);
			else
				return null;
		}

		@Override
		public String getColumnText(Object element, int col) {
			if (!(element instanceof ModuleEntry))
				return null;
			ModuleEntry entry = (ModuleEntry) element;
			Module module = entry.module;
			switch (col) {
			case 0:
				return module != null ? module.getLabel() : null;
			case 1:
				return entry.scenario;
			case 2:
				return getProductSystem(entry.productSystemId);
			case 3:
				return entry.description;
			default:
				return null;
			}
		}

		private String getProductSystem(String refId) {
			if (refId == null)
				return null;
			ProductSystemDao dao = new ProductSystemDao(Database.get());
			List<ProductSystemDescriptor> descriptors = dao.getDescriptors();
			for (ProductSystemDescriptor descriptor : descriptors) {
				if (Objects.equals(refId, descriptor.getRefId()))
					return descriptor.getName();
			}
			return null;
		}
	}

	private class ModuleModifier extends
			ComboBoxCellModifier<ModuleEntry, Module> {

		@Override
		protected Module getItem(ModuleEntry module) {
			return module.module;
		}

		@Override
		protected Module[] getItems(ModuleEntry element) {
			return Module.values();
		}

		@Override
		protected String getText(Module type) {
			if (type == null)
				return "";
			return Labels.getEnumText(type);
		}

		@Override
		protected void setItem(ModuleEntry entry, Module module) {
			if (entry.module == module)
				return;
			entry.module = module;
			editor.setDirty(true);
		}
	}

	private class ScenarioModifier extends
			ComboBoxCellModifier<ModuleEntry, String> {

		@Override
		protected String getItem(ModuleEntry module) {
			return module.scenario;
		}

		@Override
		protected String[] getItems(ModuleEntry element) {
			List<Scenario> scenarios = dataSet.scenarios;
			String[] names = new String[scenarios.size()];
			for (int i = 0; i < scenarios.size(); i++)
				names[i] = scenarios.get(i).name;
			Arrays.sort(names);
			return names;
		}

		@Override
		protected String getText(String scenario) {
			return scenario;
		}

		@Override
		protected void setItem(ModuleEntry module, String scenario) {
			if (Objects.equals(module.scenario, scenario))
				return;
			module.scenario = scenario;
			editor.setDirty(true);
		}
	}

	private class DescriptionModifier extends TextCellModifier<ModuleEntry> {

		@Override
		protected String getText(ModuleEntry module) {
			return module.description;
		}

		@Override
		protected void setText(ModuleEntry module, String text) {
			if (Objects.equals(module.description, text))
				return;
			module.description = text;
			editor.setDirty(true);
		}
	}

}