package com.greendelta.olca.plugins.oekobaudat.rcp.ui.editor;

import org.eclipse.jface.dialogs.DialogPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.greendelta.olca.plugins.oekobaudat.rcp.Messages;

public class ScenarioWizardPage extends WizardPage {

	private Text name;
	private boolean defaultScenario;
	private Text group;
	private Text description;

	private Composite container;

	public ScenarioWizardPage() {
		super("Add a secnario");
		setTitle("Add a scenario");
		setDescription("Create a new EPD for the current database");
		setPageComplete(false);
	}

	@Override
	public void createControl(Composite parent) {
		container = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 2;

		GridData gd = new GridData(GridData.FILL_HORIZONTAL);

		Label nameLabel = new Label(container, SWT.NONE);
		nameLabel.setText(Messages.Name);
		name = new Text(container, SWT.BORDER | SWT.SINGLE);
		name.addModifyListener(new TextListener());
		name.setLayoutData(gd);

		Label groupLabel = new Label(container, SWT.NONE);
		groupLabel.setText(Messages.Group);
		group = new Text(container, SWT.BORDER | SWT.SINGLE);
		group.addModifyListener(new TextListener());
		group.setLayoutData(gd);

		Label descriptionLabel = new Label(container, SWT.NONE);
		descriptionLabel.setText(Messages.Description);
		description = new Text(container, SWT.BORDER | SWT.SINGLE);
		description.addModifyListener(new TextListener());
		description.setLayoutData(gd);

		setControl(container);
		setPageComplete(false);
	}

	public String getName() {
		return name.getText();
	}

	private void validateInput() {
		boolean valid = validateName(name.getText());
		if (valid) {
			setMessage(null);
			setPageComplete(true);
		}
	}

	private boolean validateName(String name) {
		if (name == null || name.trim().isEmpty()) {
			error("Invalid name");
			return false;
		}
		else
			return true;
	}

	private void error(String string) {
		setMessage(string, DialogPage.ERROR);
		setPageComplete(false);
	}

	private class TextListener implements ModifyListener {
		@Override
		public void modifyText(ModifyEvent e) {
			validateInput();
		}
	}
}
