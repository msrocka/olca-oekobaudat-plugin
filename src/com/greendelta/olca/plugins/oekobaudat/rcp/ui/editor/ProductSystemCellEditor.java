package com.greendelta.olca.plugins.oekobaudat.rcp.ui.editor;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.openlca.app.components.DialogCellEditor;
import org.openlca.app.components.ModelSelectionDialog;
import org.openlca.core.model.ModelType;
import org.openlca.core.model.descriptors.BaseDescriptor;

import com.greendelta.olca.plugins.oekobaudat.model.ModuleEntry;
import com.greendelta.olca.plugins.oekobaudat.rcp.Messages;

/**
 * A dialog cell editor for selecting the product system for a module entry.
 */
class ProductSystemCellEditor extends DialogCellEditor {

	private EpdEditor editor;
	private ModuleEntry entry;

	ProductSystemCellEditor(Composite parent, EpdEditor editor) {
		super(parent);
		this.editor = editor;
	}

	@Override
	protected Object openDialogBox(Control window) {
		BaseDescriptor descriptor = ModelSelectionDialog
				.select(ModelType.PRODUCT_SYSTEM);
		if (entry == null)
			return null;
		if (descriptor == null)
			entry.productSystemId = null;
		else
			entry.productSystemId = descriptor.getRefId();
		editor.setDirty(true);
		return entry;
	}

	@Override
	protected void doSetValue(Object value) {
		if (!(value instanceof ModuleEntry)) {
			super.doSetValue(value);
			return;
		}
		entry = (ModuleEntry) value;
		super.doSetValue(Messages.SelectAProductSystem);
	}

}
