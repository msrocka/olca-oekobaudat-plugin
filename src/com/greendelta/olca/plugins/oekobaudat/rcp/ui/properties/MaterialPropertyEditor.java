package com.greendelta.olca.plugins.oekobaudat.rcp.ui.properties;

import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.forms.editor.FormEditor;
import org.openlca.app.editors.IEditor;
import org.openlca.app.util.Editors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.greendelta.olca.plugins.oekobaudat.io.Configs;
import com.greendelta.olca.plugins.oekobaudat.io.EpdStore;
import com.greendelta.olca.plugins.oekobaudat.model.MaterialProperty;
import com.greendelta.olca.plugins.oekobaudat.rcp.Plugin;
import com.greendelta.olca.plugins.oekobaudat.rcp.ui.SimpleEditorInput;

public class MaterialPropertyEditor extends FormEditor implements IEditor {

	private static final String ID = "MaterialPropertyEditor";

	private Logger log = LoggerFactory.getLogger(getClass());

	private List<MaterialProperty> properties;
	private boolean dirty;

	public static void open() {
		Editors.open(new SimpleEditorInput("Material properties"), ID);
	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		super.init(site, input);
		EpdStore store = Plugin.getEpdStore();
		properties = Configs.getMaterialProperties(store);
	}

	@Override
	public void setDirty(boolean b) {
		if (dirty != b) {
			dirty = b;
			editorDirtyStateChanged();
		}
	}

	@Override
	public boolean isDirty() {
		return dirty;
	}

	@Override
	protected void addPages() {
		try {
			addPage(new MaterialPropertyPage(this, properties));
		} catch (Exception e) {
			log.error("failed to add editor page", e);
		}
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		EpdStore store = Plugin.getEpdStore();
		Configs.save(properties, store);
		setDirty(false);
	}

	@Override
	public void doSaveAs() {
		setDirty(false);
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

}
