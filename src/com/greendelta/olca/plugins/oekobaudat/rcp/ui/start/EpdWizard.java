package com.greendelta.olca.plugins.oekobaudat.rcp.ui.start;

import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.openlca.app.util.Error;
import org.openlca.app.util.UI;
import org.openlca.core.model.descriptors.FlowDescriptor;

import com.greendelta.olca.plugins.oekobaudat.io.EpdStore;
import com.greendelta.olca.plugins.oekobaudat.rcp.Plugin;

public class EpdWizard extends Wizard {

	protected EpdWizardPage page;

	public static void open() {
		EpdStore epdStore = Plugin.getEpdStore();
		if (epdStore == null) {
			Error.showBox("No active database",
					"Please activate a database to create an EPD");
			return;
		}
		WizardDialog wizardDialog = new WizardDialog(UI.shell(),
				new EpdWizard());
		wizardDialog.open();
	}

	@Override
	public void addPages() {
		page = new EpdWizardPage();
		addPage(page);
	}

	@Override
	public boolean performFinish() {
		EpdStore store = Plugin.getEpdStore();
		if (store == null)
			return false;
		String name = page.getEpdName();
		FlowDescriptor flow = page.getFlow();
		new EpdCreationJob(name, flow).schedule();
		return true;
	}
}
