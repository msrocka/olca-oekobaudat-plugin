package com.greendelta.olca.plugins.oekobaudat.rcp;

import com.greendelta.olca.plugins.oekobaudat.model.Indicator;
import com.greendelta.olca.plugins.oekobaudat.model.Module;
import com.greendelta.olca.plugins.oekobaudat.model.SubType;

public class Labels {

	private Labels() {
	}

	public static SubType getSubType(String value) {
		if (value.equals(Messages.Average))
			return SubType.AVERAGE;
		if (value.equals(Messages.Generic))
			return SubType.GENERIC;
		if (value.equals(Messages.Representative))
			return SubType.REPRESENTATIVE;
		if (value.equals(Messages.Specific))
			return SubType.SPECIFIC;
		if (value.equals(Messages.Template))
			return SubType.TEMPLATE;
		return null;
	}

	public static String getEnumText(Object enumValue) {
		if (enumValue instanceof Indicator)
			return Labels.epdIndicator((Indicator) enumValue);
		if (enumValue instanceof Module)
			return ((Module) enumValue).getLabel();
		if (enumValue instanceof SubType)
			return Labels.subtype((SubType) enumValue);
		return null;
	}

	public static String epdIndicator(Indicator indicator) {
		if (indicator == null)
			return "unknown";
		String name = indicator.name();
		String key = "Indicator_" + name;
		String val = Messages.getMap().get(key);
		return val == null ? "unknown" : val;
	}

	public static String subtype(SubType subtype) {
		if (subtype == null)
			return null;
		switch (subtype) {
		case AVERAGE:
			return Messages.Average;
		case GENERIC:
			return Messages.Generic;
		case REPRESENTATIVE:
			return Messages.Representative;
		case SPECIFIC:
			return Messages.Specific;
		case TEMPLATE:
			return Messages.Template;
		default:
			return Messages.None;
		}
	}

}
