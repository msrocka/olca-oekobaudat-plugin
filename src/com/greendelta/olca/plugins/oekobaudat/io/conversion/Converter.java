package com.greendelta.olca.plugins.oekobaudat.io.conversion;

import org.openlca.ilcd.processes.Process;

import com.greendelta.olca.plugins.oekobaudat.io.EpdStore;
import com.greendelta.olca.plugins.oekobaudat.io.MappingConfig;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDataSet;

public class Converter {

	public static String NAMESPACE = "http://www.iai.kit.edu/EPD/2013";
	public static String NAMESPACE_OLCA = "http://openlca.org/epd_ilcd";

	private Converter() {
	}

	public static EpdDataSet convert(Process process, MappingConfig config, String[] langs) {
		return new ProcessConverter(process, config).convert(langs);
	}

	public static Process convert(EpdDataSet dataSet, MappingConfig config) {
		return new EpdConverter(dataSet, config).convert();
	}

	public static void writeProductData(EpdDataSet dataSet, EpdStore store) {
		new FlowDecorator(dataSet.declaredProduct, store).write();
	}

	public static void readProductData(EpdDataSet dataSet, EpdStore store) {
		new FlowDecorator(dataSet.declaredProduct, store).read();
	}

}
