package com.greendelta.olca.plugins.oekobaudat.io.conversion;

import org.openlca.core.model.descriptors.ActorDescriptor;
import org.openlca.core.model.descriptors.SourceDescriptor;
import org.openlca.ilcd.commons.DataSetReference;
import org.openlca.ilcd.commons.DataSetType;
import org.openlca.ilcd.commons.LangString;
import org.openlca.ilcd.commons.Other;
import org.openlca.ilcd.commons.ProcessType;
import org.openlca.ilcd.processes.ComplianceDeclaration;
import org.openlca.ilcd.processes.ComplianceDeclarationList;
import org.openlca.ilcd.processes.LCIMethod;
import org.openlca.ilcd.processes.ModellingAndValidation;
import org.openlca.ilcd.processes.Process;
import org.openlca.ilcd.processes.Representativeness;
import org.openlca.ilcd.processes.Validation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.greendelta.olca.plugins.oekobaudat.io.EpdStore;
import com.greendelta.olca.plugins.oekobaudat.model.ModellingInfo;
import com.greendelta.olca.plugins.oekobaudat.model.Review;
import com.greendelta.olca.plugins.oekobaudat.model.SubType;

class ModellingInfoConverter {

	static ModellingInfo read(Process process, String[] langs) {
		return new Reader(process, langs).read();
	}

	static void write(Process process, ModellingInfo info) {
		new Writer(process, info).write();
	}

	private static class Reader {

		private Process process;
		private String[] langs;

		Reader(Process process, String[] langs) {
			this.process = process;
			this.langs = langs;
		}

		ModellingInfo read() {
			if (process == null)
				return null;
			ModellingInfo info = new ModellingInfo();
			ModellingAndValidation mav = process.modellingAndValidation;
			if (mav == null)
				return info;
			readType(mav, info);
			readDataSources(mav, info);
			readReviews(mav, info);
			return info;
		}

		private void readType(ModellingAndValidation mav, ModellingInfo info) {
			LCIMethod method = mav.lciMethod;
			if (method == null || method.other == null)
				return;
			Element typeElement = Util.getElement(method.other, "subType");
			if (typeElement != null) {
				SubType type = SubType.fromLabel(typeElement.getTextContent());
				info.subType = type;
			}
			for (DataSetReference ref : method.referenceToLCAMethodDetails) {
				SourceDescriptor source = Util.readRef(SourceDescriptor.class,
						ref, langs);
				info.methodDetails.add(source);
			}
		}

		private void readDataSources(ModellingAndValidation mav,
				ModellingInfo info) {
			Representativeness repri = mav.representativeness;
			if (repri == null)
				return;
			String useAdvice = LangString.getFirst(repri.useAdviceForDataSet, langs);
			info.useAdvice = useAdvice;
			for (DataSetReference ref : repri.referenceToDataSource) {
				SourceDescriptor descriptor = Util.readRef(
						SourceDescriptor.class, ref, langs);
				info.sources.add(descriptor);
			}
		}

		private void readReviews(ModellingAndValidation mav, ModellingInfo info) {
			Validation validation = mav.validation;
			if (validation == null)
				return;
			for (org.openlca.ilcd.processes.Review iReview : validation.review) {
				Review oReview = new Review();
				info.reviews.add(oReview);
				oReview.type = iReview.type;
				String details = LangString.getFirst(iReview.otherReviewDetails, langs);
				oReview.details = details;
				SourceDescriptor report = Util.readRef(SourceDescriptor.class,
						iReview.referenceToCompleteReviewReport, langs);
				oReview.reviewReport = report;
				for (DataSetReference ref : iReview.referenceToNameOfReviewerAndInstitution) {
					ActorDescriptor reviewer = Util.readRef(
							ActorDescriptor.class, ref, langs);
					oReview.reviewers.add(reviewer);
				}
			}
		}
	}

	private static class Writer {

		private Process process;
		private ModellingInfo info;

		Writer(Process process, ModellingInfo info) {
			this.process = process;
			this.info = info;
		}

		void write() {
			if (process == null || info == null)
				return;
			ModellingAndValidation mav = new ModellingAndValidation();
			process.modellingAndValidation = mav;
			writeCompliance(mav);
			writeLciMethod(mav);
			writeDataSources(mav);
			writeReviews(mav);
		}

		private void writeLciMethod(ModellingAndValidation mav) {
			LCIMethod method = new LCIMethod();
			mav.lciMethod = method;
			method.processType = ProcessType.EPD;
			for (SourceDescriptor source : info.methodDetails) {
				DataSetReference ref = Util.createRef(source);
				method.referenceToLCAMethodDetails.add(ref);
			}
			SubType subType = info.subType;
			if (subType == null)
				return;
			Other other = new Other();
			method.other = other;
			Document doc = Util.createDocument();
			Element element = doc.createElementNS(Converter.NAMESPACE,
					"epd:subType");
			other.getAny().add(element);
			element.setTextContent(subType.getLabel());
		}

		private void writeCompliance(ModellingAndValidation mav) {
			ComplianceDeclarationList list = new ComplianceDeclarationList();
			mav.complianceDeclarations = list;
			ComplianceDeclaration declaration = new ComplianceDeclaration();
			list.complianceDeclatations.add(declaration);
			DataSetReference ref = new DataSetReference();
			ref.type = DataSetType.SOURCE_DATA_SET;
			ref.uri = "../sources/b00f9ec0-7874-11e3-981f-0800200c9a66";
			ref.uuid = "b00f9ec0-7874-11e3-981f-0800200c9a66";
			LangString.set(ref.shortDescription, "DIN EN 15804", EpdStore.lang());
			declaration.referenceToComplianceSystem = ref;
		}

		private void writeDataSources(ModellingAndValidation mav) {
			Representativeness repri = new Representativeness();
			mav.representativeness = repri;
			if (info.useAdvice != null)
				LangString.set(repri.useAdviceForDataSet,
						info.useAdvice, EpdStore.lang());
			for (SourceDescriptor descriptor : info.sources) {
				DataSetReference ref = Util.createRef(descriptor);
				repri.referenceToDataSource.add(ref);
			}
		}

		private void writeReviews(ModellingAndValidation mav) {
			if (info.reviews.isEmpty())
				return;
			Validation validation = new Validation();
			mav.validation = validation;
			for (Review oReview : info.reviews) {
				org.openlca.ilcd.processes.Review iReview = new org.openlca.ilcd.processes.Review();
				validation.review.add(iReview);
				iReview.type = oReview.type;
				LangString.set(iReview.otherReviewDetails,
						oReview.details, EpdStore.lang());
				DataSetReference reportRef = Util.createRef(
						oReview.reviewReport);
				iReview.referenceToCompleteReviewReport = reportRef;
				for (ActorDescriptor descriptor : oReview.reviewers) {
					DataSetReference ref = Util.createRef(descriptor);
					iReview.referenceToNameOfReviewerAndInstitution.add(ref);
				}
			}
		}
	}
}
