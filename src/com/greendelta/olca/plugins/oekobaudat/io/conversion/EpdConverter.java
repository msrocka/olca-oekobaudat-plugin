package com.greendelta.olca.plugins.oekobaudat.io.conversion;

import org.openlca.core.model.descriptors.FlowDescriptor;
import org.openlca.ilcd.commons.DataSetReference;
import org.openlca.ilcd.commons.DataSetType;
import org.openlca.ilcd.commons.LangString;
import org.openlca.ilcd.commons.Other;
import org.openlca.ilcd.commons.QuantitativeReferenceType;
import org.openlca.ilcd.processes.DataSetInfo;
import org.openlca.ilcd.processes.Exchange;
import org.openlca.ilcd.processes.Process;
import org.openlca.ilcd.processes.ProcessInfo;
import org.openlca.ilcd.processes.QuantitativeReference;
import org.w3c.dom.Document;

import com.greendelta.olca.plugins.oekobaudat.io.EpdStore;
import com.greendelta.olca.plugins.oekobaudat.io.MappingConfig;
import com.greendelta.olca.plugins.oekobaudat.model.DeclaredProduct;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDataSet;

/**
 * Converts an EPD to an ILCD process data set
 */
class EpdConverter {

	private final EpdDataSet dataSet;
	private final MappingConfig config;
	private Process process;

	public EpdConverter(EpdDataSet dataSet, MappingConfig config) {
		this.dataSet = dataSet;
		this.config = config;
	}

	public Process convert() {
		if (dataSet == null)
			return null;
		process = new Process();
		process.version = "1.1";
		mapDeclaredProduct(dataSet);
		ModellingInfoConverter.write(process, dataSet.modellingInfo);
		AdminInfoConverter.write(process, dataSet.adminInfo);
		DataSetInfoConverter.write(process, dataSet.dataSetInfo);
		ResultConverter.writeResults(dataSet, process, config);
		addEpdExtensions();
		return process;
	}

	private void mapDeclaredProduct(EpdDataSet dataSet) {
		DeclaredProduct product = dataSet.declaredProduct;
		if (product == null || product.flow == null)
			return;
		FlowDescriptor flow = product.flow;
		QuantitativeReference qRef = new QuantitativeReference();
		qRef.type = QuantitativeReferenceType.REFERENCE_FLOWS;
		qRef.referenceToReferenceFlow.add(0);
		ProcessInfo processInfo = getProcessInfo();
		processInfo.quantitativeReference = qRef;
		createRefExchange(flow, 0, product.amount);
	}

	private void createRefExchange(FlowDescriptor flow, int refId,
			double amount) {
		Exchange exchange = new Exchange();
		exchange.resultingAmount = amount;
		exchange.meanAmount = amount;
		exchange.id = refId;
		DataSetReference ref = new DataSetReference();
		exchange.flow = ref;
		ref.uuid = flow.getRefId();
		ref.uri = "../flows/" + flow.getRefId();
		ref.type = DataSetType.FLOW_DATA_SET;
		LangString.set(ref.shortDescription, flow.getName(),
				EpdStore.lang());
		process.exchanges.add(exchange);
	}

	private void addEpdExtensions() {
		Document document = Util.createDocument();
		ProcessInfo processInfo = getProcessInfo();
		DataSetInfo dataSetInfo = processInfo.dataSetInformation;
		if (dataSetInfo == null) {
			dataSetInfo = new DataSetInfo();
			processInfo.dataSetInformation = dataSetInfo;
		}
		Other other = dataSetInfo.other;
		if (other == null) {
			other = new Other();
			dataSetInfo.other = other;
		}
		ModuleConverter.writeModules(dataSet, other, document);
		ScenarioConverter.writeScenarios(dataSet, other, document);
	}

	private ProcessInfo getProcessInfo() {
		ProcessInfo processInfo = process.processInfo;
		if (processInfo == null) {
			processInfo = new ProcessInfo();
			process.processInfo = processInfo;
		}
		return processInfo;
	}
}