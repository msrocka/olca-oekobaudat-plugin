package com.greendelta.olca.plugins.oekobaudat.io.conversion;

import java.util.List;
import java.util.Objects;

import org.openlca.core.model.FlowType;
import org.openlca.core.model.descriptors.FlowDescriptor;
import org.openlca.ilcd.commons.DataSetReference;
import org.openlca.ilcd.commons.LangString;
import org.openlca.ilcd.commons.Other;
import org.openlca.ilcd.processes.Exchange;
import org.openlca.ilcd.processes.Process;
import org.openlca.ilcd.processes.ProcessInfo;
import org.openlca.ilcd.processes.QuantitativeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.greendelta.olca.plugins.oekobaudat.io.MappingConfig;
import com.greendelta.olca.plugins.oekobaudat.model.AdminInfo;
import com.greendelta.olca.plugins.oekobaudat.model.Amount;
import com.greendelta.olca.plugins.oekobaudat.model.DataSetInfo;
import com.greendelta.olca.plugins.oekobaudat.model.DeclaredProduct;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDataSet;
import com.greendelta.olca.plugins.oekobaudat.model.IndicatorResult;
import com.greendelta.olca.plugins.oekobaudat.model.ModellingInfo;
import com.greendelta.olca.plugins.oekobaudat.model.ModuleEntry;
import com.greendelta.olca.plugins.oekobaudat.model.Scenario;

/**
 * Converts an ILCD process data set to an EPD data set.
 */
class ProcessConverter {

	private Logger log = LoggerFactory.getLogger(getClass());

	private final Process process;
	private final MappingConfig config;
	private String[] langs;

	public ProcessConverter(Process process, MappingConfig config) {
		this.process = process;
		this.config = config;
	}

	public EpdDataSet convert(String[] langs) {
		if (process == null)
			return null;
		this.langs = langs;
		EpdDataSet dataSet = new EpdDataSet();
		DataSetInfo dataSetInfo = DataSetInfoConverter.read(process, langs);
		dataSet.dataSetInfo = dataSetInfo;
		ModellingInfo modellingInfo = ModellingInfoConverter.read(process, langs);
		dataSet.modellingInfo = modellingInfo;
		AdminInfo adminInfo = AdminInfoConverter.read(process, langs);
		dataSet.adminInfo = adminInfo;
		mapDeclaredProduct(dataSet);
		mapExtensions(dataSet);
		mapResults(dataSet);
		return dataSet;
	}

	private void mapDeclaredProduct(EpdDataSet dataSet) {
		DeclaredProduct product = new DeclaredProduct();
		dataSet.declaredProduct = product;
		Exchange exchange = getProductExchange();
		if (exchange == null) {
			log.warn("could not find a reference flow in data set {}", dataSet);
			return;
		}
		product.flow = getFlowDescriptor(exchange);
		if (exchange.resultingAmount != null)
			product.amount = exchange.resultingAmount;
	}

	private Exchange getProductExchange() {
		ProcessInfo processInfo = process.processInfo;
		if (processInfo == null)
			return null;
		QuantitativeReference qRef = processInfo.quantitativeReference;
		if (qRef == null || qRef.referenceToReferenceFlow.isEmpty())
			return null;
		Integer id = qRef.referenceToReferenceFlow.get(0);
		if (id == null)
			return null;
		for (Exchange exchange : process.exchanges) {
			if (id == exchange.id)
				return exchange;
		}
		return null;
	}

	private FlowDescriptor getFlowDescriptor(Exchange exchange) {
		DataSetReference ref = exchange.flow;
		if (ref == null)
			return null;
		FlowDescriptor flow = new FlowDescriptor();
		flow.setFlowType(FlowType.PRODUCT_FLOW);
		flow.setRefId(ref.uuid);
		String name = LangString.getFirst(ref.shortDescription, langs);
		flow.setName(name);
		return flow;
	}

	private void mapExtensions(EpdDataSet dataSet) {
		ProcessInfo processInfo = process.processInfo;
		if (processInfo == null)
			return;
		org.openlca.ilcd.processes.DataSetInfo dataSetInfo = processInfo.dataSetInformation;
		if (dataSetInfo == null || dataSetInfo.other == null)
			return;
		Other other = dataSetInfo.other;
		List<Scenario> scenarios = ScenarioConverter.readScenarios(other);
		dataSet.scenarios.addAll(scenarios);
		List<ModuleEntry> modules = ModuleConverter.readModules(other);
		dataSet.moduleEntries.addAll(modules);
	}

	private void mapResults(EpdDataSet dataSet) {
		List<IndicatorResult> results = ResultConverter.readResults(process,
				config);
		dataSet.results.addAll(results);
		// data sets may not have the module-entry extension, thus we have to
		// find the module entries for such data sets from the results
		for (IndicatorResult result : results) {
			for (Amount amount : result.amounts) {
				ModuleEntry entry = findModuleEntry(dataSet, amount);
				if (entry != null)
					continue;
				entry = new ModuleEntry();
				entry.module = amount.module;
				entry.scenario = amount.scenario;
				dataSet.moduleEntries.add(entry);
			}
		}
	}

	private ModuleEntry findModuleEntry(EpdDataSet dataSet, Amount amount) {
		for (ModuleEntry entry : dataSet.moduleEntries) {
			if (Objects.equals(entry.module, amount.module)
					&& Objects
							.equals(entry.scenario, amount.scenario))
				return entry;
		}
		return null;
	}
}
