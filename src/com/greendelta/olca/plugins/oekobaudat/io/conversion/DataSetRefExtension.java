package com.greendelta.olca.plugins.oekobaudat.io.conversion;

import org.openlca.core.model.ModelType;
import org.openlca.core.model.descriptors.ActorDescriptor;
import org.openlca.core.model.descriptors.BaseDescriptor;
import org.openlca.core.model.descriptors.FlowDescriptor;
import org.openlca.core.model.descriptors.SourceDescriptor;
import org.openlca.ilcd.commons.Other;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Creates a data set reference element in an extension point (see the generic
 * product or vendor information in the extended product data sets).
 */
class DataSetRefExtension {

	private String type;
	private String path;
	private String tagName;

	public static FlowDescriptor readFlow(String tagName, Other extension) {
		DataSetRefExtension ext = new DataSetRefExtension(tagName,
				ModelType.FLOW);
		return ext.read(FlowDescriptor.class, extension);
	}

	public static ActorDescriptor readActor(String tagName, Other extension) {
		DataSetRefExtension ext = new DataSetRefExtension(tagName,
				ModelType.ACTOR);
		return ext.read(ActorDescriptor.class, extension);
	}

	public static SourceDescriptor readSource(String tagName, Other extension) {
		DataSetRefExtension ext = new DataSetRefExtension(tagName,
				ModelType.SOURCE);
		return ext.read(SourceDescriptor.class, extension);
	}

	public static <T extends BaseDescriptor> void write(T descriptor,
			String tagName, Other extension) {
		if (tagName == null || extension == null)
			return;
		Element old = Util.getElement(extension, tagName);
		if (old != null)
			extension.getAny().remove(old);
		if (descriptor == null)
			return;
		DataSetRefExtension ext = new DataSetRefExtension(tagName,
				descriptor.getModelType());
		ext.write(descriptor, extension);
	}

	private DataSetRefExtension(String tagName, ModelType modelType) {
		this.tagName = tagName;
		initTypeAndPath(modelType);
	}

	private void initTypeAndPath(ModelType modelType) {
		if (modelType == null) {
			type = "other external file";
			path = "unknown";
		}
		switch (modelType) {
		case ACTOR:
			type = "contact data set";
			path = "contacts";
			break;
		case SOURCE:
			type = "source data set";
			path = "sources";
			break;
		case FLOW:
			type = "flow data set";
			path = "flows";
			break;
		case IMPACT_METHOD:
			type = "LCIA method data set";
			path = "lciamethods";
			break;
		case PROCESS:
			type = "process data set";
			path = "processes";
			break;
		case FLOW_PROPERTY:
			type = "flow property data set";
			path = "flowproperties";
			break;
		case UNIT_GROUP:
			type = "unit group data set";
			path = "unitgroups";
			break;
		default:
			type = "other external file";
			path = "unknown";
			break;
		}
	}

	private <T extends BaseDescriptor> T read(Class<T> clazz, Other other) {
		Element element = Util.getElement(other, tagName);
		if (element == null)
			return null;
		try {
			T d = clazz.newInstance();
			d.setRefId(element.getAttribute("refObjectId"));
			Element descriptionElement = Util.getChild(element,
					"shortDescription");
			if (descriptionElement != null)
				d.setName(descriptionElement.getTextContent());
			return d;
		} catch (Exception e) {
			Logger log = LoggerFactory.getLogger(getClass());
			log.error("failed to create descriptor instance for " + clazz, e);
			return null;
		}
	}

	private void write(BaseDescriptor d, Other other) {
		if (other == null)
			return;
		Element element = Util.getElement(other, tagName);
		if (element != null)
			other.getAny().remove(element);
		element = createElement(d);
		if (element == null)
			return;
		other.getAny().add(element);
	}

	private Element createElement(BaseDescriptor d) {
		Document doc = Util.createDocument();
		if (doc == null || d == null)
			return null;
		Element e = doc.createElementNS(Converter.NAMESPACE, "epd:" + tagName);
		e.setAttribute("type", type);
		e.setAttribute("refObjectId", d.getRefId());
		e.setAttribute("uri", "../" + path + "/" + d.getRefId());
		Element descriptionElement = doc.createElementNS(
				"http://lca.jrc.it/ILCD/Common", "common:shortDescription");
		e.appendChild(descriptionElement);
		descriptionElement.setTextContent(d.getName());
		return e;
	}
}
