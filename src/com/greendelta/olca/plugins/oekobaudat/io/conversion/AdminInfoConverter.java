package com.greendelta.olca.plugins.oekobaudat.io.conversion;

import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.openlca.core.model.descriptors.ActorDescriptor;
import org.openlca.ilcd.commons.DataSetReference;
import org.openlca.ilcd.commons.DataSetType;
import org.openlca.ilcd.commons.LangString;
import org.openlca.ilcd.processes.DataEntry;
import org.openlca.ilcd.processes.Process;
import org.openlca.ilcd.processes.Publication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.greendelta.olca.plugins.oekobaudat.io.EpdStore;
import com.greendelta.olca.plugins.oekobaudat.model.AdminInfo;

class AdminInfoConverter {

	static AdminInfo read(Process process, String[] langs) {
		return new Reader(process, langs).read();
	}

	static void write(Process process, AdminInfo adminInfo) {
		new Writer(process, adminInfo).write();
	}

	private static class Reader {

		private Process process;
		private String[] langs;

		Reader(Process process, String[] langs) {
			this.process = process;
			this.langs = langs;
		}

		AdminInfo read() {
			if (process == null)
				return null;
			AdminInfo info = new AdminInfo();
			org.openlca.ilcd.processes.AdminInfo information = process.administrativeInformation;
			if (information == null)
				return info;
			read(information.dataEntry, info);
			read(information.publication, info);
			return info;
		}

		private void read(DataEntry dataEntry, AdminInfo info) {
			if (dataEntry == null)
				return;
			XMLGregorianCalendar cal = dataEntry.timeStamp;
			if (cal != null)
				info.lastUpdate = cal.toGregorianCalendar().getTime();
			ActorDescriptor documentor = Util.readRef(ActorDescriptor.class,
					dataEntry.referenceToPersonOrEntityEnteringTheData, langs);
			info.documentor = documentor;
		}

		private void read(Publication publication, AdminInfo info) {
			if (publication == null)
				return;
			info.version = publication.dataSetVersion;
			if (publication.copyright != null)
				info.copyright = publication.copyright;
			String accessRestrictions = LangString.getFirst(
					publication.accessRestrictions, langs);
			info.accessRestrictions = accessRestrictions;
			ActorDescriptor owner = Util.readRef(ActorDescriptor.class,
					publication.referenceToOwnershipOfDataSet, langs);
			info.owner = owner;
		}

	}

	private static class Writer {

		private Process process;
		private AdminInfo info;

		Writer(Process process, AdminInfo info) {
			this.process = process;
			this.info = info;
		}

		void write() {
			if (process == null || info == null)
				return;
			org.openlca.ilcd.processes.AdminInfo information = new org.openlca.ilcd.processes.AdminInfo();
			process.administrativeInformation = information;
			DataEntry dataEntry = new DataEntry();
			information.dataEntry = dataEntry;
			write(dataEntry);
			Publication publication = new Publication();
			information.publication = publication;
			write(publication);
		}

		private void write(DataEntry dataEntry) {
			setTimeStamp(dataEntry);
			setDataFormats(dataEntry);
			DataSetReference documentor = Util.createRef(info.documentor);
			dataEntry.referenceToPersonOrEntityEnteringTheData = documentor;
		}

		private void setTimeStamp(DataEntry dataEntry) {
			if (info.lastUpdate == null)
				return;
			try {
				GregorianCalendar cal = new GregorianCalendar();
				cal.setTime(info.lastUpdate);
				XMLGregorianCalendar xmlCal = DatatypeFactory.newInstance()
						.newXMLGregorianCalendar(cal);
				dataEntry.timeStamp = xmlCal;
			} catch (Exception e) {
				Logger log = LoggerFactory.getLogger(getClass());
				log.error("failed to set time stamp", e);
			}
		}

		private void setDataFormats(DataEntry dataEntry) {
			DataSetReference ref = new DataSetReference();
			dataEntry.referenceToDataSetFormat.add(ref);
			ref.uuid = "a97a0155-0234-4b87-b4ce-a45da52f2a40";
			ref.uri = "../sources/ILCD_Format_" +
					"a97a0155-0234-4b87-b4ce-a45da52f2a40.xml";
			ref.type = DataSetType.SOURCE_DATA_SET;
			LangString.set(ref.shortDescription,
					"ILCD format 1.1", "en");

			ref = new DataSetReference();
			dataEntry.referenceToDataSetFormat.add(ref);
			ref.uuid = "cba73800-7874-11e3-981f-0800200c9a66";
			ref.uri = "../sources/cba73800-7874-11e3-981f-0800200c9a66.xml";
			ref.type = DataSetType.SOURCE_DATA_SET;
			LangString.set(ref.shortDescription,
					"EPD Data Format Extensions", "en");
		}

		private void write(Publication publication) {
			publication.dataSetVersion = info.version;
			DataSetReference owner = Util.createRef(info.owner);
			publication.referenceToOwnershipOfDataSet = owner;
			publication.copyright = info.copyright;
			LangString.set(publication.accessRestrictions,
					info.accessRestrictions, EpdStore.lang());
		}

	}
}
