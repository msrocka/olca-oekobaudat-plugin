package com.greendelta.olca.plugins.oekobaudat.io.conversion;

import java.util.Objects;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.openlca.core.model.descriptors.BaseDescriptor;
import org.openlca.ilcd.commons.DataSetReference;
import org.openlca.ilcd.commons.DataSetType;
import org.openlca.ilcd.commons.LangString;
import org.openlca.ilcd.commons.Other;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.greendelta.olca.plugins.oekobaudat.io.EpdStore;

/**
 * Utility methods for data set conversions.
 */
class Util {

	private Util() {
	}

	static <T extends BaseDescriptor> T readRef(Class<T> clazz,
			DataSetReference ref, String[] langs) {
		if (ref == null)
			return null;
		try {
			T d = clazz.newInstance();
			d.setName(LangString.getFirst(ref.shortDescription, langs));
			d.setRefId(ref.uuid);
			return d;
		} catch (Exception e) {
			Logger log = LoggerFactory.getLogger(Util.class);
			log.error("failed to create descriptor from reference " + ref, e);
			return null;
		}
	}

	static DataSetReference createRef(BaseDescriptor d) {
		if (d == null)
			return null;
		DataSetReference ref = new DataSetReference();
		ref.type = getRefType(d);
		ref.uri = getRefPath(d) + d.getRefId() + ".xml";
		ref.uuid = d.getRefId();
		LangString.set(ref.shortDescription, d.getName(),
				EpdStore.lang());
		return ref;
	}

	private static DataSetType getRefType(BaseDescriptor d) {
		if (d == null || d.getModelType() == null)
			return null;
		switch (d.getModelType()) {
		case ACTOR:
			return DataSetType.CONTACT_DATA_SET;
		case SOURCE:
			return DataSetType.SOURCE_DATA_SET;
		case UNIT_GROUP:
			return DataSetType.UNIT_GROUP_DATA_SET;
		case FLOW_PROPERTY:
			return DataSetType.FLOW_PROPERTY_DATA_SET;
		case FLOW:
			return DataSetType.FLOW_DATA_SET;
		case PROCESS:
			return DataSetType.PROCESS_DATA_SET;
		case IMPACT_METHOD:
			return DataSetType.LCIA_METHOD_DATA_SET;
		default:
			return DataSetType.OTHER_EXTERNAL_FILE;
		}
	}

	private static String getRefPath(BaseDescriptor d) {
		if (d == null || d.getModelType() == null)
			return "../other/";
		switch (d.getModelType()) {
		case ACTOR:
			return "../contacts/";
		case SOURCE:
			return "../sources/";
		case UNIT_GROUP:
			return "../unitgroups/";
		case FLOW_PROPERTY:
			return "../flowproperties/";
		case FLOW:
			return "../flows/";
		case PROCESS:
			return "../processes/";
		case IMPACT_METHOD:
			return "../lciamethods/";
		default:
			return "../other/";
		}
	}

	static boolean hasNull(Object... vals) {
		for (Object val : vals) {
			if (val == null)
				return true;
		}
		return false;
	}

	static Document createDocument() {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			return db.newDocument();
		} catch (Exception e) {
			Logger log = LoggerFactory.getLogger(Util.class);
			log.error("failed to init DOM doc", e);
			return null;
		}
	}

	static Element createElement(Other extension, String tagName) {
		Document doc = createDocument();
		if (extension == null || doc == null)
			return null;
		return doc.createElementNS(Converter.NAMESPACE, "epd:" + tagName);
	}

	static Double getDoubleContent(NodeList nodeList) {
		String text = getTextContent(nodeList);
		if (text == null)
			return null;
		try {
			return Double.parseDouble(text);
		} catch (Exception e) {
			Logger log = LoggerFactory.getLogger(Util.class);
			log.error("content of {} is not numeric", nodeList);
			return null;
		}
	}

	static String getTextContent(NodeList nodeList) {
		Node node = getFirstNode(nodeList);
		if (node == null)
			return null;
		return node.getTextContent();
	}

	static Node getFirstNode(NodeList nodeList) {
		if (nodeList == null || nodeList.getLength() == 0)
			return null;
		return nodeList.item(0);
	}

	static Element getElement(Other extension, String tagName) {
		if (extension == null || tagName == null)
			return null;
		for (Object any : extension.getAny()) {
			if (!(any instanceof Element))
				continue;
			Element element = (Element) any;
			if (Objects.equals(tagName, element.getLocalName()))
				return element;
		}
		return null;
	}

	static Element getChild(Element root, String... path) {
		if (root == null || path.length == 0)
			return null;
		Element element = root;
		for (String tagName : path) {
			if (element == null)
				return null;
			NodeList list = element.getChildNodes();
			if (list == null || list.getLength() == 0)
				return null;
			element = null;
			for (int i = 0; i < list.getLength(); i++) {
				Node node = list.item(i);
				if (!(node instanceof Element))
					continue;
				Element child = (Element) node;
				if (Objects.equals(child.getLocalName(), tagName)) {
					element = child;
					break;
				}
			}
		}
		return element;
	}

}
