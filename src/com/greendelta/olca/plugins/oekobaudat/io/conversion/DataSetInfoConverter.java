package com.greendelta.olca.plugins.oekobaudat.io.conversion;

import java.math.BigInteger;

import org.openlca.core.model.descriptors.SourceDescriptor;
import org.openlca.ilcd.commons.ClassificationInfo;
import org.openlca.ilcd.commons.DataSetReference;
import org.openlca.ilcd.commons.LangString;
import org.openlca.ilcd.commons.Other;
import org.openlca.ilcd.commons.Time;
import org.openlca.ilcd.processes.Geography;
import org.openlca.ilcd.processes.Location;
import org.openlca.ilcd.processes.Process;
import org.openlca.ilcd.processes.ProcessInfo;
import org.openlca.ilcd.processes.ProcessName;
import org.openlca.ilcd.processes.Technology;

import com.greendelta.olca.plugins.oekobaudat.io.EpdStore;
import com.greendelta.olca.plugins.oekobaudat.model.DataSetInfo;
import com.greendelta.olca.plugins.oekobaudat.model.SafetyMargins;

class DataSetInfoConverter {

	static DataSetInfo read(Process process, String[] langs) {
		return new Reader(process, langs).read();
	}

	static void write(Process process, DataSetInfo info) {
		new Writer(process, info).write();
	}

	private static class Reader {

		private Process process;
		private String[] langs;

		Reader(Process process, String[] langs) {
			this.process = process;
			this.langs = langs;
		}

		DataSetInfo read() {
			if (process == null)
				return null;
			DataSetInfo info = new DataSetInfo();
			ProcessInfo processInfo = process.processInfo;
			if (processInfo == null)
				return info;
			readDataInfo(processInfo, info);
			readTime(processInfo, info);
			readGeography(processInfo, info);
			readTechnology(processInfo, info);
			return info;
		}

		private void readDataInfo(ProcessInfo processInfo, DataSetInfo info) {
			org.openlca.ilcd.processes.DataSetInfo dataInfo = processInfo.dataSetInformation;
			if (dataInfo == null)
				return;
			info.uuid = dataInfo.uuid;
			String comment = LangString.getFirst(dataInfo.generalComment, langs);
			info.description = comment;
			SafetyMargins margins = SafetyMarginsConverter.read(dataInfo.other);
			info.safetyMargins = margins;
			info.synonyms = LangString.getFirst(dataInfo.synonyms, langs);
			readExternalDocs(dataInfo, info);
			readClassInfo(dataInfo, info);
			ProcessName name = dataInfo.name;
			if (name != null) {
				info.baseName = LangString.getFirst(name.baseName, langs);
				info.quantitativeProperties = LangString.getFirst(
						name.functionalUnitFlowProperties, langs);
			}
		}

		private void readClassInfo(
				org.openlca.ilcd.processes.DataSetInfo dataInfo,
				DataSetInfo info) {
			ClassificationInfo classInfo = dataInfo.classificationInformation;
			if (classInfo == null)
				return;
			info.classifications.addAll(classInfo.classifications);
		}

		private void readExternalDocs(
				org.openlca.ilcd.processes.DataSetInfo dataInfo,
				DataSetInfo info) {
			for (DataSetReference ref : dataInfo.externalDocumentationSources) {
				SourceDescriptor d = Util.readRef(SourceDescriptor.class, ref, langs);
				info.externalDocs.add(d);
			}
		}

		private void readTime(ProcessInfo processInfo, DataSetInfo info) {
			Time time = processInfo.time;
			if (time == null)
				return;
			BigInteger refYear = time.referenceYear;
			if (refYear != null)
				info.referenceYear = refYear.intValue();
			BigInteger validUntil = time.validUntil;
			if (validUntil != null)
				info.validUntil = validUntil.intValue();
			info.timeComment = LangString.getFirst(time.description, langs);
		}

		private void readGeography(ProcessInfo processInfo, DataSetInfo info) {
			Geography geography = processInfo.geography;
			if (geography == null || geography.location == null)
				return;
			Location location = geography.location;
			info.geographyComment = LangString.getFirst(location.description, langs);
			info.locationCode = location.location;
		}

		private void readTechnology(ProcessInfo processInfo, DataSetInfo info) {
			Technology technology = processInfo.technology;
			if (technology == null)
				return;
			String comment = LangString.getFirst(
					technology.technologyDescriptionAndIncludedProcesses, langs);
			info.technology = comment;
			String applicability = LangString.getFirst(
					technology.technologicalApplicability, langs);
			info.technologicalApplicability = applicability;
			info.pictogram = Util.readRef(SourceDescriptor.class,
					technology.referenceToTechnologyPictogramme, langs);
			for (DataSetReference ref : technology.referenceToTechnologyFlowDiagrammOrPicture) {
				SourceDescriptor d = Util.readRef(SourceDescriptor.class, ref, langs);
				info.pictures.add(d);
			}
		}
	}

	private static class Writer {

		private Process process;
		private DataSetInfo info;

		Writer(Process process, DataSetInfo info) {
			this.process = process;
			this.info = info;
		}

		void write() {
			if (process == null || info == null)
				return;
			ProcessInfo processInfo = process.processInfo;
			if (processInfo == null) {
				processInfo = new ProcessInfo();
				process.processInfo = processInfo;
			}
			writeDataInfo(processInfo);
			writeTime(processInfo);
			writeGeography(processInfo);
			writeTechnology(processInfo);
		}

		private void writeDataInfo(ProcessInfo processInfo) {
			org.openlca.ilcd.processes.DataSetInfo dataInfo = processInfo.dataSetInformation;
			if (dataInfo == null) {
				dataInfo = new org.openlca.ilcd.processes.DataSetInfo();
				processInfo.dataSetInformation = dataInfo;
			}
			dataInfo.uuid = info.uuid;
			ProcessName processName = new ProcessName();
			dataInfo.name = processName;
			LangString.set(processName.baseName, info.baseName,
					EpdStore.lang());
			LangString.set(processName.functionalUnitFlowProperties,
					info.quantitativeProperties, EpdStore.lang());
			LangString.set(dataInfo.generalComment,
					info.description, EpdStore.lang());
			LangString.set(dataInfo.synonyms, info.synonyms,
					EpdStore.lang());
			writeClassInfo(dataInfo);
			writeExternalDocs(dataInfo);
			writeSafetyMargins(dataInfo);
		}

		private void writeClassInfo(
				org.openlca.ilcd.processes.DataSetInfo dataInfo) {
			ClassificationInfo classInfo = dataInfo.classificationInformation;
			if (classInfo == null) {
				classInfo = new ClassificationInfo();
				dataInfo.classificationInformation = classInfo;
			}
			classInfo.classifications.addAll(info.classifications);
		}

		private void writeExternalDocs(
				org.openlca.ilcd.processes.DataSetInfo dataInfo) {
			for (SourceDescriptor d : info.externalDocs) {
				DataSetReference ref = Util.createRef(d);
				dataInfo.externalDocumentationSources.add(ref);
			}
		}

		private void writeSafetyMargins(
				org.openlca.ilcd.processes.DataSetInfo dataInfo) {
			if (info.safetyMargins == null)
				return;
			Other other = dataInfo.other;
			if (other == null) {
				other = new Other();
				dataInfo.other = other;
			}
			SafetyMarginsConverter.write(info, other);
		}

		private void writeTime(ProcessInfo processInfo) {
			Time time = new Time();
			processInfo.time = time;
			Integer refYear = info.referenceYear;
			if (refYear != null)
				time.referenceYear = new BigInteger(refYear.toString());
			Integer validUntil = info.validUntil;
			if (validUntil != null)
				time.validUntil = new BigInteger(validUntil.toString());
			String comment = info.timeComment;
			if (comment != null)
				LangString.set(time.description, comment, EpdStore.lang());
		}

		private void writeGeography(ProcessInfo processInfo) {
			String comment = info.geographyComment;
			String code = info.locationCode;
			if (comment == null && code == null)
				return;
			Geography geography = new Geography();
			processInfo.geography = geography;
			Location location = new Location();
			geography.location = location;
			LangString.set(location.description, comment, EpdStore.lang());
			location.location = code;
		}

		private void writeTechnology(ProcessInfo processInfo) {
			Technology technology = new Technology();
			processInfo.technology = technology;
			LangString.set(
					technology.technologyDescriptionAndIncludedProcesses,
					info.technology, EpdStore.lang());
			LangString.set(technology.technologicalApplicability,
					info.technologicalApplicability, EpdStore.lang());
			DataSetReference pictogram = Util.createRef(info.pictogram);
			technology.referenceToTechnologyPictogramme = pictogram;
			for (SourceDescriptor picture : info.pictures) {
				technology.referenceToTechnologyFlowDiagrammOrPicture.add(
						Util.createRef(picture));
			}
		}
	}
}
