package com.greendelta.olca.plugins.oekobaudat.io.server;

import java.util.Objects;
import java.util.Set;

import org.openlca.core.database.ActorDao;
import org.openlca.core.database.FlowDao;
import org.openlca.core.database.SourceDao;
import org.openlca.core.model.Actor;
import org.openlca.core.model.descriptors.FlowDescriptor;
import org.openlca.ilcd.commons.DataSetReference;
import org.openlca.ilcd.contacts.Contact;
import org.openlca.ilcd.flowproperties.FlowProperty;
import org.openlca.ilcd.flows.Flow;
import org.openlca.ilcd.flows.FlowPropertyRef;
import org.openlca.ilcd.io.FileStore;
import org.openlca.ilcd.io.NetworkClient;
import org.openlca.ilcd.processes.Process;
import org.openlca.ilcd.sources.Source;
import org.openlca.io.ilcd.output.ActorExport;
import org.openlca.io.ilcd.output.FlowExport;
import org.openlca.io.ilcd.output.SourceExport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.greendelta.olca.plugins.oekobaudat.io.Configs;
import com.greendelta.olca.plugins.oekobaudat.io.EpdStore;
import com.greendelta.olca.plugins.oekobaudat.io.MappingConfig;
import com.greendelta.olca.plugins.oekobaudat.io.conversion.Converter;
import com.greendelta.olca.plugins.oekobaudat.model.DeclaredProduct;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDataSet;

class Upload {

	private Logger log = LoggerFactory.getLogger(getClass());

	private final NetworkClient webStore;
	private final FileStore fileStore;
	private final EpdStore store;

	public Upload(NetworkClient client, EpdStore store) {
		this.webStore = client;
		this.store = store;
		fileStore = store.ilcdStore;
	}

	public void doIt(EpdDataSet dataSet) {
		uploadProducts(dataSet);
		uploadEpd(dataSet);
		uploadActorsAndSources(dataSet);
	}

	private void uploadProducts(EpdDataSet dataSet) {
		try {
			DeclaredProduct product = dataSet.declaredProduct;
			if (product != null) {
				uploadFlow(product.flow);
				uploadFlow(product.genericFlow);
			}
		} catch (Exception e) {
			log.error("failed to upload data set", e);
		}
	}

	private void uploadEpd(EpdDataSet dataSet) {
		log.trace("upload EPD: {}", dataSet);
		try {
			MappingConfig config = Configs.getMappingConfig(store);
			Process process = Converter.convert(dataSet, config);
			webStore.put(process, dataSet.dataSetInfo.uuid);
		} catch (Exception e) {
			log.error("failed to upload EPD data set", e);
		}
	}

	private void uploadFlow(FlowDescriptor flowDescriptor) {
		if (flowDescriptor == null)
			return;
		String id = flowDescriptor.getRefId();
		try {
			log.trace("upload flow {}", id);
			if (fileStore.contains(Flow.class, id)) {
				Flow flow = fileStore.get(Flow.class, id);
				webStore.put(flow, id); // considers updated versions
				syncFlow(flow);
			} else {
				FlowDao dao = new FlowDao(store.database);
				org.openlca.core.model.Flow olcaFlow = dao.getForRefId(id);
				FlowExport export = new FlowExport(store.getExportConfig(webStore));
				export.run(olcaFlow);
			}
		} catch (Exception e) {
			log.error("failed to upload flow " + id, e);
		}
	}

	private void syncFlow(Flow flow) {
		if (flow == null || flow.flowProperties == null)
			return;
		DataStoreSync sync = new DataStoreSync(fileStore, webStore);
		for (FlowPropertyRef ref : flow.flowProperties.flowProperty) {
			DataSetReference propRef = ref.flowProperty;
			if (propRef == null)
				continue;
			sync.run(FlowProperty.class, propRef.uuid);
		}
	}

	private void uploadActorsAndSources(EpdDataSet dataSet) {
		if (dataSet == null)
			return;
		Set<Ref> refs = new RefTraversal().traverse(dataSet);
		for (Ref ref : refs) {
			if (Objects.equals(ref.type, Contact.class))
				uploadActor(ref);
			else if (Objects.equals(ref.type, Source.class))
				uploadSource(ref);
		}
	}

	private void uploadActor(Ref ref) {
		if (ref == null || !ref.isValid())
			return;
		try {
			if (webStore.contains(Contact.class, ref.id))
				return;
			log.trace("upload actor {}", ref);
			if (fileStore.contains(Contact.class, ref.id)) {
				DataStoreSync sync = new DataStoreSync(fileStore, webStore);
				sync.run(Contact.class, ref.id);
			} else {
				ActorDao dao = new ActorDao(store.database);
				Actor actor = dao.getForRefId(ref.id);
				ActorExport export = new ActorExport(store.getExportConfig(webStore));
				export.run(actor);
			}
		} catch (Exception e) {
			log.error("failed to upload actor " + ref, e);
		}
	}

	private void uploadSource(Ref ref) {
		if (ref == null || !ref.isValid())
			return;
		try {
			if (webStore.contains(Source.class, ref.id))
				return;
			log.trace("upload source {}", ref);
			if (fileStore.contains(Source.class, ref.id)) {
				DataStoreSync sync = new DataStoreSync(fileStore, webStore);
				sync.run(Source.class, ref.id);
			} else {
				SourceDao dao = new SourceDao(store.database);
				org.openlca.core.model.Source olcaSource = dao
						.getForRefId(ref.id);
				SourceExport export = new SourceExport(store.getExportConfig(webStore));
				export.run(olcaSource);
			}
		} catch (Exception e) {
			log.error("failed to upload source " + ref, e);
		}
	}
}
