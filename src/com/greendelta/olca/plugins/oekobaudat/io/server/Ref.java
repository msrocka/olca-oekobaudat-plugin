package com.greendelta.olca.plugins.oekobaudat.io.server;

import java.util.Objects;

import org.openlca.core.model.descriptors.BaseDescriptor;
import org.openlca.ilcd.commons.DataSetReference;
import org.openlca.ilcd.contacts.Contact;
import org.openlca.ilcd.flowproperties.FlowProperty;
import org.openlca.ilcd.flows.Flow;
import org.openlca.ilcd.methods.LCIAMethod;
import org.openlca.ilcd.sources.Source;
import org.openlca.ilcd.units.UnitGroup;

/**
 * Describes a reference to a data set via the UUID (reference ID) and ILCD type
 * of the data set.
 */
class Ref {

	final String id;
	final Class<?> type;

	public Ref(Class<?> type, String id) {
		this.id = id;
		this.type = type;
	}

	public static Ref of(Class<?> clazz, String id) {
		return new Ref(clazz, id);
	}

	public static Ref of(DataSetReference reference) {
		if (reference == null)
			return null;
		return of(getRefClass(reference), reference.uuid);
	}

	public static Ref of(BaseDescriptor descriptor) {
		if (descriptor == null)
			return null;
		return of(getRefClass(descriptor), descriptor.getRefId());
	}

	private static Class<?> getRefClass(DataSetReference reference) {
		if (reference == null || reference.type == null)
			return null;
		switch (reference.type) {
		case LCIA_METHOD_DATA_SET:
			return LCIAMethod.class;
		case PROCESS_DATA_SET:
			return org.openlca.ilcd.processes.Process.class;
		case SOURCE_DATA_SET:
			return Source.class;
		case CONTACT_DATA_SET:
			return Contact.class;
		case FLOW_DATA_SET:
			return Flow.class;
		case FLOW_PROPERTY_DATA_SET:
			return FlowProperty.class;
		case UNIT_GROUP_DATA_SET:
			return UnitGroup.class;
		default:
			return null;
		}
	}

	private static Class<?> getRefClass(BaseDescriptor descriptor) {
		if (descriptor == null || descriptor.getModelType() == null)
			return null;
		switch (descriptor.getModelType()) {
		case ACTOR:
			return Contact.class;
		case SOURCE:
			return Source.class;
		case UNIT_GROUP:
			return UnitGroup.class;
		case FLOW_PROPERTY:
			return FlowProperty.class;
		case FLOW:
			return Flow.class;
		case PROCESS:
			return org.openlca.ilcd.processes.Process.class;
		case IMPACT_METHOD:
			return LCIAMethod.class;
		default:
			return null;
		}
	}

	public boolean isValid() {
		return id != null && type != null;
	}

	@Override
	public int hashCode() {
		return Objects.hash(type, id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj == this)
			return true;
		if (!(obj instanceof Ref))
			return false;
		Ref other = (Ref) obj;
		return Objects.equals(this.type, other.type)
				&& Objects.equals(this.id, other.id);
	}

	@Override
	public String toString() {
		return "Ref [type=" + type + ", id=" + id + "]";
	}
}