package com.greendelta.olca.plugins.oekobaudat.io.server;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openlca.ilcd.io.DataStore;
import org.openlca.ilcd.sources.Source;
import org.openlca.ilcd.util.SourceBag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Synchronizes a data set and its dependencies between two ILCD data stores.
 */
public class DataStoreSync {

	private Logger log = LoggerFactory.getLogger(getClass());

	private final DataStore source;
	private final DataStore target;

	private final Queue<Ref> queue = new ArrayDeque<>();
	private final Set<Ref> handled = new HashSet<>();

	public DataStoreSync(DataStore source, DataStore target) {
		this.source = source;
		this.target = target;
	}

	public void run(Class<?> clazz, String id) {
		log.trace("synchronize resource tree for {}:{}", clazz, id);
		try {
			queue.add(Ref.of(clazz, id));
			exec();
		} catch (Exception e) {
			log.error("failed to sync resources of " + clazz + ":" + id, e);
		} finally {
			queue.clear();
			handled.clear();
		}
	}

	private void exec() {
		while (!queue.isEmpty()) {
			Ref ref = queue.poll();
			handled.add(ref);
			Object model = sync(ref);
			if (model == null)
				continue;
			Set<Ref> nextRefs = new RefTraversal().traverse(model);
			for (Ref nextRef : nextRefs) {
				if (!handled.contains(nextRef) && !queue.contains(nextRef))
					queue.add(nextRef);
			}
		}
	}

	private Object sync(Ref ref) {
		try {
			if (target.contains(ref.type, ref.id))
				return null;
			Object model = source.get(ref.type, ref.id);
			if (model == null)
				return null;
			if (model instanceof Source)
				syncSource((Source) model, ref);
			target.put(model, ref.id);
			return model;
		} catch (Exception e) {
			log.error("failed to sync " + ref, e);
			return null;
		}
	}

	private void syncSource(Source model, Ref ref) throws Exception {
		File file = downloadFile(model);
		if (file == null)
			target.put(model, ref.id);
		else {
			target.put(model, ref.id, file);
			log.trace("delete directory {}", file.getParentFile());
			FileUtils.deleteDirectory(file.getParentFile());
		}
	}

	private File downloadFile(Source sourceDataSet) {
		try {
			SourceBag bag = new SourceBag(sourceDataSet, "en");
			if (bag.getExternalFileURIs().isEmpty())
				return null;
			String uri = bag.getExternalFileURIs().get(0);
			String fileName = new File(uri).getName();
			File dir = Files.createTempDirectory("epd_downloads").toFile();
			File tempFile = new File(dir, fileName);
			log.trace("download file to {}", tempFile);
			try (InputStream in = source.getExternalDocument(bag.getId(),
					fileName)) {
				Files.copy(in, tempFile.toPath(),
						StandardCopyOption.REPLACE_EXISTING);
			}
			return tempFile;
		} catch (Exception e) {
			log.warn("Failed to download external file", e);
			return null;
		}
	}
}
