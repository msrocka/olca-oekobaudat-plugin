package com.greendelta.olca.plugins.oekobaudat.io;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.openlca.app.preferencepages.IoPreference;
import org.openlca.core.database.IDatabase;
import org.openlca.ilcd.io.DataStore;
import org.openlca.ilcd.io.FileStore;
import org.openlca.ilcd.processes.Process;
import org.openlca.io.ilcd.input.ImportConfig;
import org.openlca.io.ilcd.output.ExportConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.greendelta.olca.plugins.oekobaudat.io.conversion.Converter;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDataSet;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDescriptor;

public class EpdStore implements Closeable {

	private Logger log = LoggerFactory.getLogger(getClass());

	/** @deprecated we are adding multi-language support... */
	public static String lang = "en";

	public final File baseDir;
	public final FileStore ilcdStore;
	public final IDatabase database;

	public EpdStore(IDatabase database, String subFolder) {
		this.database = database;
		File dbDir = database.getFileStorageLocation();
		baseDir = new File(dbDir, subFolder);
		if (!baseDir.exists())
			baseDir.mkdirs();
		ilcdStore = new FileStore(baseDir);
	}

	public EpdStore(File dir) {
		baseDir = dir;
		if (!baseDir.exists())
			baseDir.mkdirs();
		ilcdStore = new FileStore(baseDir);
		database = null;
	}

	/**
	 * Creates a configuration for exporting data sets from the local database
	 * to the given target ILCD data store.
	 */
	public ExportConfig getExportConfig(DataStore target) {
		ExportConfig config = new ExportConfig(database, target);
		config.lang = IoPreference.getIlcdLanguage();
		return config;
	}

	/**
	 * Creates a configuration for importing data sets from the given ILCD data
	 * store into the local database.
	 */
	public ImportConfig getImportConfig(DataStore source) {
		ImportConfig config = new ImportConfig(source, database);
		if (!"en".equals(lang))
			config.langs = new String[] { lang, "en" };
		return config;
	}

	/** Returns the language code for the text fields in the data sets. */
	public static String lang() {
		return lang;
	}

	@Override
	public void close() throws IOException {
		log.trace("close EPD store");
	}

	public boolean delete(EpdDescriptor descriptor) {
		if (descriptor == null)
			return false;
		try {
			log.trace("delete EPD {}", descriptor);
			return ilcdStore.delete(Process.class, descriptor.refId);
		} catch (Exception e) {
			log.error("failed to delete EPD " + descriptor, e);
			return false;
		}
	}

	public boolean contains(EpdDescriptor descriptor) {
		if (descriptor == null)
			return false;
		try {
			return ilcdStore.contains(Process.class, descriptor.refId);
		} catch (Exception e) {
			log.error("failed to check if EPD is available: " + descriptor, e);
			return false;
		}
	}

	public EpdDataSet open(EpdDescriptor descriptor) {
		try {
			log.trace("open EPD data set {}", descriptor);
			Process process = ilcdStore.get(Process.class,
					descriptor.refId);
			MappingConfig config = Configs.getMappingConfig(this);
			String[] langs = new String[] { lang, "en" };
			EpdDataSet dataSet = Converter.convert(process, config, langs);
			Converter.readProductData(dataSet, this);
			return dataSet;
		} catch (Exception e) {
			log.error("failed to open EPD data set " + descriptor, e);
			return null;
		}
	}

	public void save(EpdDataSet dataSet) {
		try {
			log.trace("update EPD data set {}", dataSet);
			MappingConfig config = Configs.getMappingConfig(this);
			Process process = Converter.convert(dataSet, config);
			ilcdStore.put(process, process.processInfo.dataSetInformation.uuid);
			Converter.writeProductData(dataSet, this);
		} catch (Exception e) {
			log.error("failed to save EPD data set " + dataSet, e);
		}
	}

	public List<EpdDescriptor> getDescriptors() {
		log.trace("get EPD descriptors");
		try {
			File dir = ilcdStore.getFolder(Process.class);
			if (dir == null)
				return Collections.emptyList();
			EpdDescriptorCollector collector = collectDescriptions(dir);
			return collector.getDescriptors();
		} catch (Exception e) {
			log.error("failed to list EPD descriptors", e);
			return Collections.emptyList();
		}
	}

	private EpdDescriptorCollector collectDescriptions(File dir)
			throws Exception {
		SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
		EpdDescriptorCollector collector = new EpdDescriptorCollector();
		for (File file : dir.listFiles()) {
			if (!file.getName().toLowerCase().endsWith(".xml")) {
				log.warn("there is a non-XML file in the process "
						+ "folder: {}", file);
				continue;
			}
			parser.parse(file, collector);
		}
		return collector;
	}
}
