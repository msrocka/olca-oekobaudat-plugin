package com.greendelta.olca.plugins.oekobaudat.model;

import java.util.ArrayList;
import java.util.List;

import org.openlca.core.model.descriptors.SourceDescriptor;

public class ModellingInfo {

	public SubType subType;
	public String useAdvice;
	public final List<Review> reviews = new ArrayList<>();
	public final List<SourceDescriptor> methodDetails = new ArrayList<>();
	public final List<SourceDescriptor> sources = new ArrayList<>();

	@Override
	public ModellingInfo clone() {
		ModellingInfo clone = new ModellingInfo();
		clone.subType = subType;
		clone.useAdvice = useAdvice;
		for (Review r : reviews)
			clone.reviews.add(r.clone());
		for (SourceDescriptor d : methodDetails)
			clone.methodDetails.add(d);
		for (SourceDescriptor d : sources)
			clone.sources.add(d);
		return clone;
	}
}
