package com.greendelta.olca.plugins.oekobaudat.model;

import java.util.Date;

import org.openlca.core.model.descriptors.ActorDescriptor;

public class AdminInfo {

	public Date lastUpdate;
	public String version;
	public ActorDescriptor documentor;
	public ActorDescriptor owner;
	public boolean copyright;
	public String accessRestrictions;

	@Override
	public AdminInfo clone() {
		AdminInfo clone = new AdminInfo();
		clone.lastUpdate = lastUpdate;
		clone.version = version;
		clone.documentor = documentor;
		clone.owner = owner;
		clone.copyright = copyright;
		clone.accessRestrictions = accessRestrictions;
		return clone;
	}

}
