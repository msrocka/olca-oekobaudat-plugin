package com.greendelta.olca.plugins.oekobaudat.model;

/**
 * Groups of the environmental parameters defined in DIN 15804 (see section
 * 7.2).
 */
public enum IndicatorGroup {

	/**
	 * See section 7.2.3, table 3
	 */
	ENVIRONMENTAL,

	/**
	 * See section 7.2.4, table 4
	 */
	RESOURCE_USE,

	/**
	 * See section 7.2.5, table 5
	 */
	WASTE_DISPOSAL,

	/**
	 * See section 7.2.5, table 5
	 */
	OUTPUT_FLOWS

}
