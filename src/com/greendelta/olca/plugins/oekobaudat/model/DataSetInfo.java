package com.greendelta.olca.plugins.oekobaudat.model;

import java.util.ArrayList;
import java.util.List;

import org.openlca.core.model.descriptors.SourceDescriptor;
import org.openlca.ilcd.commons.Classification;

public class DataSetInfo {

	public String uuid;
	public String description;
	public String baseName;
	public String quantitativeProperties;
	public String synonyms;
	public SafetyMargins safetyMargins;
	public Integer referenceYear;
	public Integer validUntil;
	public String timeComment;
	public String locationCode;
	public String geographyComment;
	public String technology;
	public String technologicalApplicability;
	public SourceDescriptor pictogram;
	public final List<Classification> classifications = new ArrayList<>();
	public final List<SourceDescriptor> externalDocs = new ArrayList<>();
	public final List<SourceDescriptor> pictures = new ArrayList<>();

	@Override
	public DataSetInfo clone() {
		DataSetInfo clone = new DataSetInfo();
		clone.uuid = uuid;
		clone.description = description;
		clone.baseName = baseName;
		clone.quantitativeProperties = quantitativeProperties;
		clone.synonyms = synonyms;
		if (safetyMargins != null)
			clone.safetyMargins = safetyMargins.clone();
		clone.referenceYear = referenceYear;
		clone.validUntil = validUntil;
		clone.timeComment = timeComment;
		clone.locationCode = locationCode;
		clone.geographyComment = geographyComment;
		clone.technology = technology;
		clone.technologicalApplicability = technologicalApplicability;
		clone.pictogram = pictogram;
		for (Classification c : classifications)
			clone.classifications.add(c);
		for (SourceDescriptor d : externalDocs)
			clone.externalDocs.add(d);
		for (SourceDescriptor d : pictures)
			clone.pictures.add(d);
		return clone;
	}

}
