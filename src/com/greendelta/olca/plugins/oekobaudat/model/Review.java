package com.greendelta.olca.plugins.oekobaudat.model;

import java.util.ArrayList;
import java.util.List;

import org.openlca.core.model.descriptors.ActorDescriptor;
import org.openlca.core.model.descriptors.SourceDescriptor;
import org.openlca.ilcd.commons.ReviewType;

public class Review {

	public String details;
	public ReviewType type;
	public SourceDescriptor reviewReport;
	public final List<ActorDescriptor> reviewers = new ArrayList<>();

	@Override
	public Review clone() {
		Review clone = new Review();
		clone.details = details;
		clone.type = type;
		clone.reviewReport = reviewReport;
		for (ActorDescriptor d : reviewers) {
			clone.reviewers.add(d);
		}
		return clone;
	}
}
