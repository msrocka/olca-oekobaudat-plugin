package com.greendelta.olca.plugins.oekobaudat.model;

import java.util.ArrayList;
import java.util.List;

public class EpdDataSet {

	public DataSetInfo dataSetInfo;
	public ModellingInfo modellingInfo;
	public AdminInfo adminInfo;
	public DeclaredProduct declaredProduct;
	public final List<IndicatorResult> results = new ArrayList<>();
	public final List<ModuleEntry> moduleEntries = new ArrayList<>();
	public final List<Scenario> scenarios = new ArrayList<>();

	public IndicatorResult getResult(Indicator indicator) {
		for (IndicatorResult result : results)
			if (result.indicator == indicator)
				return result;
		return null;
	}

	public EpdDescriptor toDescriptor() {
		EpdDescriptor descriptor = new EpdDescriptor();
		if (dataSetInfo == null)
			return descriptor;
		descriptor.name = dataSetInfo.baseName;
		descriptor.refId = dataSetInfo.uuid;
		return descriptor;
	}

	@Override
	public EpdDataSet clone() {
		EpdDataSet clone = new EpdDataSet();
		if (dataSetInfo != null)
			clone.dataSetInfo = dataSetInfo.clone();
		if (modellingInfo != null)
			clone.modellingInfo = modellingInfo.clone();
		if (adminInfo != null)
			clone.adminInfo = adminInfo.clone();
		if (declaredProduct != null)
			clone.declaredProduct = declaredProduct.clone();
		for (IndicatorResult r : results)
			clone.results.add(r.clone());
		for (ModuleEntry e : moduleEntries)
			clone.moduleEntries.add(e.clone());
		for (Scenario s : scenarios)
			clone.scenarios.add(s.clone());
		return clone;
	}
}
