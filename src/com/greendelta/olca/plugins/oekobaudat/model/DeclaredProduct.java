package com.greendelta.olca.plugins.oekobaudat.model;

import java.util.ArrayList;

import org.openlca.core.model.descriptors.ActorDescriptor;
import org.openlca.core.model.descriptors.FlowDescriptor;
import org.openlca.core.model.descriptors.SourceDescriptor;

public class DeclaredProduct {

	public double amount = 1;
	public FlowDescriptor flow;
	public FlowDescriptor genericFlow;
	public boolean vendorSpecific;
	public ActorDescriptor vendor;
	public SourceDescriptor documentation;
	public String version;
	public final ArrayList<MaterialPropertyValue> properties = new ArrayList<>();

	@Override
	public DeclaredProduct clone() {
		DeclaredProduct clone = new DeclaredProduct();
		clone.amount = amount;
		clone.flow = flow;
		clone.genericFlow = genericFlow;
		clone.vendorSpecific = vendorSpecific;
		clone.vendor = vendor;
		clone.documentation = documentation;
		clone.version = version;
		for (MaterialPropertyValue v : properties) {
			clone.properties.add(v);
		}
		return clone;
	}
}
