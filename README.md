Oekobaudat plugin for openLCA
=============================
This is a plugin for creating EPDs for the Oekobaudat database (see 
http://www.nachhaltigesbauen.de/baustoff-und-gebaeudedaten/oekobaudat.html). The EPDs
are calculated with openLCA product systems and can be shared via a soda4LCA server
(http://www.iai.kit.edu/www-extern/index.php?id=soda4lca). As exchange format an
extended version of the ILCD dataformat is used (see 
http://iai-uiserv1.iai.fzk.de/EPD/doc/EPD_Developer_Docs.zip). 

# TODOS
* avoid massive EpdStore.lang() calls