'use strict';

const os = require('os');
const path = require('path');
const fs = require('fs');
const gulp = require('gulp');
const del = require('del');
const zip = require('gulp-zip');

gulp.task('default', ['html'], () => {
    return gulp.src('../dist/**')
        .pipe(zip('html.zip'))
        .pipe(gulp.dest('../'));
});

gulp.task('html', ['boots'], () => {
    return gulp.src(['*.html'])
        .pipe(gulp.dest('../dist'));
});

gulp.task('boots', ['libs'], () => {
    return gulp.src(['node_modules/bootstrap/dist/**'])
        .pipe(gulp.dest('../dist/libs/bootstrap'));
})

gulp.task('libs', ['clean'], () => {
    return gulp.src(['olca.utils.js', 'node_modules/jquery/dist/jquery.min.js',
        'node_modules/angular/angular.min.js'])
        .pipe(gulp.dest('../dist/libs'))
})

gulp.task('clean', ['clean-olca'], () => {
    return del(['../dist', '../html.zip'], { force: true });
})

gulp.task('clean-olca', () => {
    let dir = path.join(os.homedir(), 'openLCA-data-1.4', 'html',
        'com.greendelta.olca.plugins.oekobaudat');
    try {
        fs.accessSync(dir, fs.F_OK);
        return del(dir, { force: true })
    } catch (e) {
    }
})
