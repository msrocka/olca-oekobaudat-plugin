package com.greendelta.olca.plugins.oekobaudat;

import java.io.File;
import java.nio.file.Files;

import org.junit.Assert;
import org.junit.Test;

import com.greendelta.olca.plugins.oekobaudat.io.Configs;
import com.greendelta.olca.plugins.oekobaudat.io.IndicatorMapping;
import com.greendelta.olca.plugins.oekobaudat.io.MappingConfig;
import com.greendelta.olca.plugins.oekobaudat.model.Indicator;

public class MappingConfigTest {

	@Test
	public void testGetDefault() {
		MappingConfig config = Configs.getDefaultMappingConfig();
		checkMappings(config);
	}

	@Test
	public void testWriteReadFile() throws Exception {
		MappingConfig config = Configs.getDefaultMappingConfig();
		File tmpFile = Files.createTempFile("mapping_config_", ".json")
				.toFile();
		Configs.save(config, tmpFile);
		config = Configs.getMappingConfig(tmpFile);
		checkMappings(config);
		Assert.assertTrue(tmpFile.delete());
	}

	private void checkMappings(MappingConfig config) {
		Assert.assertEquals(Indicator.values().length, config.indicatorMappings.size());
		for (Indicator indicator : Indicator.values()) {
			IndicatorMapping mapping = config.getIndicatorMapping(indicator);
			Assert.assertNotNull(mapping);
			Assert.assertNotNull(mapping.indicator);
			Assert.assertNotNull(mapping.indicatorLabel);
			Assert.assertNotNull(mapping.indicatorRefId);
			Assert.assertNotNull(mapping.unitLabel);
			Assert.assertNotNull(mapping.unitRefId);
		}
	}

}
