package com.greendelta.olca.plugins.oekobaudat;

import java.io.File;
import java.nio.file.Files;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.greendelta.olca.plugins.oekobaudat.io.Configs;
import com.greendelta.olca.plugins.oekobaudat.model.MaterialProperty;

public class MaterialPropertiesTest {

	@Test
	public void testGetDefault() {
		List<MaterialProperty> properties = Configs
				.getDefaultMaterialProperties();
		checkProperties(properties);
	}

	@Test
	public void testWriteReadFile() throws Exception {
		List<MaterialProperty> properties = Configs
				.getDefaultMaterialProperties();
		File tmpFile = Files.createTempFile("material_properties", ".json")
				.toFile();
		Configs.save(properties, tmpFile);
		properties = Configs.getMaterialProperties(tmpFile);
		checkProperties(properties);
		Assert.assertTrue(tmpFile.delete());
	}

	private void checkProperties(List<MaterialProperty> properties) {
		Assert.assertEquals(2, properties.size());
		for (MaterialProperty property : properties) {
			Assert.assertTrue("pr1".equals(property.id)
					|| "pr2".equals(property.id));
			if ("pr1".equals(property.id)) {
				Assert.assertEquals("average mass per unit area",
						property.name);
				Assert.assertEquals("kg/m^2", property.unit);
			} else {
				Assert.assertEquals("raw density", property.name);
				Assert.assertEquals("kg/m^3", property.unit);
			}
		}
	}
}
