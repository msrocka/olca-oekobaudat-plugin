package com.greendelta.olca.plugins.oekobaudat.io;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.greendelta.olca.plugins.oekobaudat.model.DataSetInfo;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDataSet;
import com.greendelta.olca.plugins.oekobaudat.model.SafetyMargins;

public class SafetyMarginsTest {

	@Rule
	public TemporaryFolder testFolder = new TemporaryFolder();

	private EpdStore store;

	@Before
	public void setUp() throws Exception {
		store = new EpdStore(testFolder.getRoot());
	}

	@Test
	public void testNoMargins() {
		EpdDataSet dataSet = createDataSet();
		store.save(dataSet);
		dataSet = store.open(dataSet.toDescriptor());
		Assert.assertNull(dataSet.dataSetInfo.safetyMargins);
	}

	@Test
	public void testMargins() {
		EpdDataSet dataSet = createDataSet();
		SafetyMargins margins = new SafetyMargins();
		margins.margins = 42d;
		margins.description = "description";
		dataSet.dataSetInfo.safetyMargins = margins;
		store.save(dataSet);
		dataSet = store.open(dataSet.toDescriptor());
		DataSetInfo info = dataSet.dataSetInfo;
		Assert.assertEquals(42, info.safetyMargins.margins, 1e-17);
		Assert.assertEquals("description", info.safetyMargins.description);
	}

	private EpdDataSet createDataSet() {
		EpdDataSet dataSet = new EpdDataSet();
		DataSetInfo info = new DataSetInfo();
		dataSet.dataSetInfo = info;
		String uuid = UUID.randomUUID().toString();
		info.uuid = uuid;
		info.baseName = "test";
		return dataSet;
	}

}
