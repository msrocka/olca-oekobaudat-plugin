package com.greendelta.olca.plugins.oekobaudat.io;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.openlca.core.model.descriptors.FlowDescriptor;

import com.greendelta.olca.plugins.oekobaudat.io.EpdStore;
import com.greendelta.olca.plugins.oekobaudat.model.DataSetInfo;
import com.greendelta.olca.plugins.oekobaudat.model.DeclaredProduct;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDataSet;

public class ProductReferenceTest {

	@Rule
	public TemporaryFolder testFolder = new TemporaryFolder();

	private EpdStore store;

	@Before
	public void setUp() throws Exception {
		store = new EpdStore(testFolder.getRoot());
	}

	@Test
	public void testNoProductRef() {
		EpdDataSet dataSet = initDataSet();
		store.save(dataSet);
		dataSet = store.open(dataSet.toDescriptor());
		Assert.assertNull(dataSet.declaredProduct.flow);
		Assert.assertEquals(1, dataSet.declaredProduct.amount, 1e-17);
	}

	@Test
	public void testProductRef() {
		EpdDataSet dataSet = initDataSet();
		DeclaredProduct product = new DeclaredProduct();
		dataSet.declaredProduct = product;
		product.amount = (double) 42;
		FlowDescriptor flow = new FlowDescriptor();
		flow.setRefId(UUID.randomUUID().toString());
		flow.setName("Flow");
		product.flow = flow;
		store.save(dataSet);
		dataSet = store.open(dataSet.toDescriptor());
		Assert.assertEquals("Flow", dataSet.declaredProduct.flow
				.getName());
		Assert.assertEquals(42, dataSet.declaredProduct.amount, 1e-17);
	}

	private EpdDataSet initDataSet() {
		EpdDataSet dataSet = new EpdDataSet();
		DataSetInfo info = new DataSetInfo();
		dataSet.dataSetInfo = info;
		info.uuid = UUID.randomUUID().toString();
		info.baseName = "Test";
		return dataSet;
	}

}
