package com.greendelta.olca.plugins.oekobaudat.io;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.greendelta.olca.plugins.oekobaudat.io.EpdStore;
import com.greendelta.olca.plugins.oekobaudat.model.DataSetInfo;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDataSet;
import com.greendelta.olca.plugins.oekobaudat.model.Scenario;

public class ScenarioTest {

	@Rule
	public TemporaryFolder testFolder = new TemporaryFolder();

	private EpdStore store;

	@Before
	public void setUp() throws Exception {
		store = new EpdStore(testFolder.getRoot());
	}

	@Test
	public void testNoScenarios() {
		EpdDataSet dataSet = createDataSet();
		store.save(dataSet);
		dataSet = store.open(dataSet.toDescriptor());
		Assert.assertTrue(dataSet.scenarios.isEmpty());
	}

	@Test
	public void testMultipleScenarios() {
		EpdDataSet dataSet = createDataSet();
		for (int i = 0; i < 10; i++) {
			Scenario scenario = new Scenario();
			scenario.name = "scenario " + i;
			scenario.description = "this is scenario " + i;
			scenario.group = "group";
			dataSet.scenarios.add(scenario);
		}
		store.save(dataSet);
		dataSet = store.open(dataSet.toDescriptor());
		Assert.assertEquals(10, dataSet.scenarios.size());
		for (Scenario scenario : dataSet.scenarios) {
			Assert.assertTrue(scenario.name.startsWith("scenario "));
			Assert.assertTrue(scenario.description.startsWith("this is "));
			Assert.assertEquals("group", scenario.group);
		}
	}

	private EpdDataSet createDataSet() {
		EpdDataSet dataSet = new EpdDataSet();
		String uuid = UUID.randomUUID().toString();
		DataSetInfo info = new DataSetInfo();
		dataSet.dataSetInfo = info;
		info.uuid = uuid;
		info.baseName = "test";
		return dataSet;
	}
}
