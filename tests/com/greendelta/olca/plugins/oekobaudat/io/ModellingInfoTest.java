package com.greendelta.olca.plugins.oekobaudat.io;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.openlca.core.model.descriptors.ActorDescriptor;
import org.openlca.core.model.descriptors.SourceDescriptor;

import com.greendelta.olca.plugins.oekobaudat.model.DataSetInfo;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDataSet;
import com.greendelta.olca.plugins.oekobaudat.model.ModellingInfo;
import com.greendelta.olca.plugins.oekobaudat.model.Review;
import com.greendelta.olca.plugins.oekobaudat.model.SubType;

public class ModellingInfoTest {

	@Rule
	public TemporaryFolder testFolder = new TemporaryFolder();

	private EpdDataSet dataSet;

	@Before
	public void setUp() throws Exception {
		EpdStore store = new EpdStore(testFolder.getRoot());
		EpdDataSet dataSet = createDataSet();
		store.save(dataSet);
		this.dataSet = store.open(dataSet.toDescriptor());
		store.close();
	}

	private EpdDataSet createDataSet() {
		EpdDataSet dataSet = new EpdDataSet();
		DataSetInfo dataInfo = new DataSetInfo();
		dataSet.dataSetInfo = dataInfo;
		dataInfo.uuid = UUID.randomUUID().toString();
		dataInfo.baseName = "test";
		ModellingInfo info = new ModellingInfo();
		dataSet.modellingInfo = info;
		info.subType = SubType.REPRESENTATIVE;
		info.useAdvice = "use advice";
		addSource(info);
		addReview(info);
		return dataSet;
	}

	private void addSource(ModellingInfo info) {
		SourceDescriptor source = new SourceDescriptor();
		source.setRefId("123456");
		source.setName("Test source");
		info.sources.add(source);
	}

	private void addReview(ModellingInfo info) {
		Review review = new Review();
		info.reviews.add(review);
		review.details = "review details";
		ActorDescriptor reviewer = new ActorDescriptor();
		reviewer.setRefId("654321");
		reviewer.setName("Test actor");
		review.reviewers.add(reviewer);
	}

	@Test
	public void testSubType() {
		ModellingInfo info = dataSet.modellingInfo;
		Assert.assertEquals(SubType.REPRESENTATIVE, info.subType);
	}

	@Test
	public void testSource() {
		ModellingInfo info = dataSet.modellingInfo;
		Assert.assertEquals(1, info.sources.size());
		SourceDescriptor source = info.sources.get(0);
		Assert.assertEquals("123456", source.getRefId());
		Assert.assertEquals("Test source", source.getName());
	}

	@Test
	public void testUseAdvice() {
		ModellingInfo info = dataSet.modellingInfo;
		Assert.assertEquals("use advice", info.useAdvice);
	}

	@Test
	public void testReview() {
		ModellingInfo info = dataSet.modellingInfo;
		Review review = info.reviews.get(0);
		Assert.assertEquals("review details", review.details);
		ActorDescriptor reviewer = review.reviewers.get(0);
		Assert.assertEquals("654321", reviewer.getRefId());
		Assert.assertEquals("Test actor", reviewer.getName());
	}

}
