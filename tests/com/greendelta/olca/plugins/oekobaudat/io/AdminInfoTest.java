package com.greendelta.olca.plugins.oekobaudat.io;

import java.util.Date;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.openlca.core.model.descriptors.ActorDescriptor;

import com.greendelta.olca.plugins.oekobaudat.model.AdminInfo;
import com.greendelta.olca.plugins.oekobaudat.model.DataSetInfo;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDataSet;

public class AdminInfoTest {

	@Rule
	public TemporaryFolder testFolder = new TemporaryFolder();

	private AdminInfo adminInfo;

	@Before
	public void setUp() throws Exception {
		EpdStore store = new EpdStore(testFolder.getRoot());
		EpdDataSet dataSet = createDataSet();
		store.save(dataSet);
		dataSet = store.open(dataSet.toDescriptor());
		store.close();
		adminInfo = dataSet.adminInfo;
	}

	private EpdDataSet createDataSet() {
		EpdDataSet dataSet = new EpdDataSet();
		DataSetInfo info = new DataSetInfo();
		dataSet.dataSetInfo = info;
		info.uuid = UUID.randomUUID().toString();
		info.baseName = "test";
		AdminInfo adminInfo = new AdminInfo();
		dataSet.adminInfo = adminInfo;
		adminInfo.lastUpdate = new Date();
		adminInfo.documentor = createActor();
		adminInfo.owner = createActor();
		adminInfo.version = "01.00.000";
		adminInfo.accessRestrictions = "access restrictions";
		adminInfo.copyright = true;
		return dataSet;
	}

	private ActorDescriptor createActor() {
		ActorDescriptor descriptor = new ActorDescriptor();
		descriptor.setName("Test actor");
		descriptor.setRefId("123456");
		return descriptor;
	}

	@Test
	public void testLastUpdate() {
		Assert.assertNotNull(adminInfo.lastUpdate);
	}

	@Test
	public void testActors() {
		Assert.assertNotNull(adminInfo.owner);
		Assert.assertEquals("123456", adminInfo.owner.getRefId());
		Assert.assertNotNull(adminInfo.documentor);
		Assert.assertEquals("123456", adminInfo.documentor.getRefId());
	}

	@Test
	public void testOtherFiels() {
		Assert.assertEquals("01.00.000", adminInfo.version);
		Assert.assertEquals("access restrictions",
				adminInfo.accessRestrictions);
	}

}
