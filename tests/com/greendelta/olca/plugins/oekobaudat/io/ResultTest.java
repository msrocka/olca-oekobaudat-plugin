package com.greendelta.olca.plugins.oekobaudat.io;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.greendelta.olca.plugins.oekobaudat.model.Amount;
import com.greendelta.olca.plugins.oekobaudat.model.DataSetInfo;
import com.greendelta.olca.plugins.oekobaudat.model.EpdDataSet;
import com.greendelta.olca.plugins.oekobaudat.model.Indicator;
import com.greendelta.olca.plugins.oekobaudat.model.IndicatorResult;
import com.greendelta.olca.plugins.oekobaudat.model.Module;

public class ResultTest {

	@Rule
	public TemporaryFolder testFolder = new TemporaryFolder();

	private EpdStore store;

	@Before
	public void setUp() throws Exception {
		store = new EpdStore(testFolder.getRoot());
	}

	@Test
	public void testNoResults() {
		EpdDataSet dataSet = createDataSet();
		store.save(dataSet);
		dataSet = store.open(dataSet.toDescriptor());
		Assert.assertTrue(dataSet.results.isEmpty());
	}

	@Test
	public void testResults() {
		EpdDataSet dataSet = createDataSet();
		for (Indicator indicator : Indicator.values()) {
			IndicatorResult result = new IndicatorResult();
			dataSet.results.add(result);
			result.indicator = indicator;
			Amount a1 = new Amount();
			a1.module = Module.A1;
			a1.scenario = "s1";
			a1.value = 42d;
			result.amounts.add(a1);
			Amount a2 = new Amount();
			a2.module = Module.C3;
			a2.value = 24d;
			result.amounts.add(a2);
		}
		store.save(dataSet);
		dataSet = store.open(dataSet.toDescriptor());
		checkIndicatorResults(dataSet);
	}

	private void checkIndicatorResults(EpdDataSet dataSet) {
		Assert.assertEquals(Indicator.values().length, dataSet.results
				.size());
		for (Indicator indicator : Indicator.values()) {
			IndicatorResult result = dataSet.getResult(indicator);
			checkAmount(result.amounts.get(0));
			checkAmount(result.amounts.get(1));
		}
	}

	private void checkAmount(Amount amount) {
		if (amount.module == Module.A1) {
			Assert.assertEquals("s1", amount.scenario);
			Assert.assertEquals(42, amount.value, 1e-17);
		} else {
			Assert.assertEquals(Module.C3, amount.module);
			Assert.assertEquals(24, amount.value, 1e-17);
			Assert.assertNull(amount.scenario);
		}
	}

	private EpdDataSet createDataSet() {
		EpdDataSet dataSet = new EpdDataSet();
		DataSetInfo info = new DataSetInfo();
		dataSet.dataSetInfo = info;
		String uuid = UUID.randomUUID().toString();
		info.uuid = uuid;
		info.baseName = "test";
		return dataSet;
	}

}
