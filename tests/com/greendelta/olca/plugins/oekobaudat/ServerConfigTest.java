package com.greendelta.olca.plugins.oekobaudat;

import java.io.File;
import java.nio.file.Files;

import org.junit.Assert;
import org.junit.Test;

import com.greendelta.olca.plugins.oekobaudat.io.Configs;
import com.greendelta.olca.plugins.oekobaudat.io.ServerConfig;

public class ServerConfigTest {

	@Test
	public void testGetDefault() {
		ServerConfig config = Configs.getDefaultServerConfig();
		checkConfig(config);
	}

	@Test
	public void testWriteReadFile() throws Exception {
		ServerConfig config = Configs.getDefaultServerConfig();
		File tmpFile = Files.createTempFile("server_config_", ".json").toFile();
		Configs.save(config, tmpFile);
		config = Configs.getServerConfig(tmpFile);
		checkConfig(config);
		Assert.assertTrue(tmpFile.delete());
	}

	private void checkConfig(ServerConfig config) {
		Assert.assertNotNull(config.downloadCredentials);
		Assert.assertNotNull(config.downloadCredentials.url);
		Assert.assertNotNull(config.uploadCredentials);
	}

}
